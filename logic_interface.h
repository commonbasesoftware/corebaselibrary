/**
 * @file logic_interface.h
 *
 * @date 28.09.2021
 * @author A.Niggebaum
 *
 * @brief All relevant definitions, enumerations and types to interface the base library with the logic
 *
 * @ingroup CoreBaseGroup
 */

#ifndef LOGIC_INTERFACE_H_
#define LOGIC_INTERFACE_H_

#include <stdint.h>

#include "core/timer/timer.h"
#include "core/messaging/messaging.h"
#include "core/interface/interface.h"
#include "core/lightEngine/lightEngine.h"

/**
 * Marker that a light engine is attached and not residing on a peripheral
 */
#define LOGIC_LIGHT_ENGINE_ATTACHED_MARKER		0xFF

/**
 * Defines one network group. A group is one type of peripheral. If multiple peripherals
 * are needed, multiple groups need to be defined.
 */
typedef struct NetworkGroupStruct {
	uint8_t groupID;							//!< ID of group
	uint8_t nOfExpected;						//!< number of expected members
	const uint8_t* addressIndexList;			//!< list of variables where the assigned addresses are stored
} NetworkGroupTypeDef;

/**
 * Structure that defines the complete network layout
 */
typedef struct NetworkLayoutStruct
{
	uint8_t totalNumberOfPeripherals;		//!< total number of peripherals
	uint8_t numberOfSlowTimers;				//!< number of (1s) slow timers in logic
	uint8_t numberOfLightEngines;			//!< number of light engines in system
	uint8_t* lightEngineList;				//!< list of indices (or attached marker) to addresses where the light engine is located
	uint8_t numberOfGroups;					//!< number of groups
	const NetworkGroupTypeDef** groups;		//!< pointer to start of list of NetworkGroupTypDef structures describing the network
} NetworkLayoutTypeDef;

//
//	Configuration of peripherals
//

/**
 * Defines one instruction message.
 */
typedef struct configurationElementStruct {
	uint8_t numberOfBytesInInstruction;		//!< number of bytes in message
	const uint8_t* instruction;				//!< pointer to first byte in list
} configurationElementTypeDef;

/**
 * Bundle of instruction messages of type ::configurationElementTypeDef that need to be sent to a peripheral
 */
typedef struct configurationSetStruct {
	uint8_t numberOfInstructsionInSet;						//!< number of instructions in this this set
	uint8_t targetAddressIndex;								//!< _index_ of the address to send it to
	const configurationElementTypeDef* instructionList;		//!< pointer to first element in list
} configurationSetTypeDef;

/**
 * Complete set of instructions
 */
typedef struct logicConfigurationStruct {
	uint16_t totalNumberOfConfigurationMessages;			//!< Total number of instructions in all sets combined
	uint8_t numberOfConfigurationSets;						//!< Number of configuration sets
	const configurationSetTypeDef* instructionSets;			//!< pointer to first element in list of configuration sets
} logicConfigurationTypeDef;

//
//	Persistent variables
//
/**
 * Definition of persistent variables
 */
typedef struct logicPersistentVariablesStruct {
	uint16_t nOfPersistentVariables;
	uint32_t* listOfDefaults;
} logicPersistentVariables_t;

//
//	Functions to be implemented by logic
//
/**
 * Initialisation routine for logic. Called from the underlying library once before entering the event loop
 */
void logic_start(void);

/**
 * Callback triggered when the attached light engine has finished a fade
 */
void logic_attachedLightEngineFadeFinished(void);

/**
 * Main event loop by logic. Will be called by underlying library as often as possible.
 *
 * @return Status of the loop. Signals the calling entity that an error occured if not equal to zero
 */
uint8_t logic_eventLoop(void);

//
//	Functions accessible by logic module
//
/**
 * Interface function to trigger a response to a message
 * @param msg	pointer to message that triggered the response
 * @param code	response code to send in response
 * @return		ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef coreLib_respondWithCode(ILBMessageStruct* msg, ILBErrorTypeDef code);

/**
 * Send a message into the ILB network
 * @param msg				pointer to message structure to send
 * @param callback			pointer to callback function called when a response is received
 * @param timeoutMS			time in ms before the timeout callback is triggered
 * @param timeoutCallback	pointer to callback function that should be triggered in case of a timeout
 * @return
 */
ILB_HAL_StatusTypeDef coreLib_sendMessage(ILBMessageStruct* msg, messageHandler_t callback, uint16_t timeoutMS, callbackHandle_t timeoutCallback);

/**
 * Starts a timer in the base software that triggers the callback at the end of the
 * @param timerIndex		Index of timer. Note that timers need to be registered at the time of initialisation and that there is a limited number of timers
 * @param timeoutCallback	Pointer to function to call when the timer has expired
 * @param timeoutMS			Time in ms before the timeout is triggered
 * @return					ILB_HAL_OK on success, error code if the timer could not be created
 */
ILB_HAL_StatusTypeDef coreLib_startTimer(uint8_t timerIndex, callbackHandle_t timeoutCallback, uint16_t timeoutMS);

/**
 * Starts a fade or sets the light engine output. The fade is started relative from the current position
 * @param LEIndex			Index of light engine
 * @param level				Level (0 to 255) to set
 * @param mired				Mired to output
 * @param fadeTimeMS		Fade time to new output
 * @param intensityLookup	Intensity lookup to use. A non-linear curve may be used.
 */
void coreLib_setLightEngine(uint8_t LEIndex, uint8_t level, uint16_t mired, uint32_t fadeTimeMS, IntensityLookup_t intensityLookup);

/**
 * Emits a follower command into the network, instruction all listening followers to apply the provided levels
 * @param followerGroup		Follower group to address
 * @param level				Level (0 to 255) to apply
 * @param mired				Mired to output
 * @param fadeTimeMS		Fade time in ms
 */
void coreLib_setFollower(uint8_t followerGroup, uint8_t level, uint16_t mired, uint16_t fadeTimeMS);

/**
 * Get the actual address instead of working with the index. For ease of access, the logic should use an enum list to
 * reference the peripherals. This function returns the actual address behind.
 * @param index				Index of peripehral
 * @return					Address assigned to the peripehral, 0xFF if index is out of range.
 */
uint8_t coreLib_getAddressForIndex(uint8_t index);

//
// persistent variables interface
//
/**
 * Resets the persistent variables to their default value
 */
void persistentVariables_reset(void);

/**
 * Sets a persistent variable to the supplied value. Persistent variables are addressed through indices.
 * @param index		Index of persistent variable to set
 * @param value		Value to store or update the persistent variable
 * @return			ILB_HAL_OK on success, error code if error occured
 */
ILB_HAL_StatusTypeDef persistentVariables_set(uint8_t index, uint32_t value);

/**
 * Retreive a persistent variable from the memory.
 * @param index		Index of the persistent variable to access
 * @param var		Pointer to locatio where to place the persistent variable
 * @return			ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef persistentVariables_get(uint8_t index, uint32_t* var);

/**
 * Write the persistent variables to memory. This forces a synchronisation. To reduce write cycles, the persistent variables
 * are only written in regular intervals.
 * @return			ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef persistentVariables_writeToMemory(void);

#endif /* LOGIC_INTERFACE_H_ */
