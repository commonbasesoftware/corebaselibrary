/**
 * @file unitTest_interface.h
 *
 * @date Jan 19, 2022
 * @author A.Niggebaum
 *
 * @brief Interface to be used by the unit test.
 *
 * All functions required to execute unit tests are defined here. They are implemented in this library but are normally not exposed. By including this header, the unit test mechanism gains access
 *
 * @ingroup CoreBaseLibrary
 *
 */

#ifndef CORE_UNITTEST_INTERFACE_H_
#define CORE_UNITTEST_INTERFACE_H_

//
//	Interface Base Software -> Logic
//	Functions called by the base software within the logic
//
//	These functions are implemented within the Core_Base_Library but normally not exposed.

/**
 * Initialisation of the core.
 * @param nOfSlowTimers Number of slow timers (1s resolution) required.
 * @return ::ILB_HAL_OK on success, error code otherwise
 */
ILB_HAL_StatusTypeDef core_init(uint8_t* nOfSlowTimers);

/**
 * Main state machine routine. Must be called periodically and as often as possible.
 * @return non 0x00 indicates error
 */
ILB_HAL_StatusTypeDef core_logic(void);

/**
 * Handling routine for received command messages
 * @param m Command message received
 */
void core_commandHandler(ILBMessageStruct* m);

/**
 * Handling routine for read messages
 * @param m Read message received
 */
void core_readHandler(ILBMessageStruct* m);

/**
 * Handling routine for write messages
 * @param m Write message received
 */
void core_writeHandler(ILBMessageStruct* m);

//
//	Interface Logic -> Base Software
//	Functions called by the logic to control the system
//

#endif /* CORE_UNITTEST_INTERFACE_H_ */
