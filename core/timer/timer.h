/**
 * @file timer.h
 *
 * @date 28.09.2021
 * @author A.Niggebaum
 *
 * @brief Management of timers for the logic and core tasks, also routines to start the periodic broadcast and information distribution
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_TIMER_TIMER_H_
#define CORE_TIMER_TIMER_H_

#include "ilb.h"

/**
 * Slow timers used by the core static library (1s resolution)
 */
enum coreBaseSlowTimerEnum {
	CORE_BASE_TIMER_BROADCAST,               /**< Periodic status broadcast timer. Interval is set in settings.h */
	CORE_BASE_TIMER_INFORMATION_DISTRIBUTION,/**< Periodic value writing timer. Interval is set in settings.h */
	CORE_BASE_TIME_NUMBER_OF                 /**< Number of slow timers for information distribution purposes. Used by the initialization function */
};

// helper interface for periodic broadcast messages
/**
 * Helper function to start the periodic broadcasts.
 * @param callback	Routine that arms the sending of a broadcast message
 * @param timeoutS	Interval of broadcast messages
 */
void timer_startBroadcastTimer(callbackHandle_t callback, uint16_t timeoutS);

/**
 * Helper function to start the information distribution routine
 *
 * @param callback	Routine sending the information to peripherals that rely on this mechanism. Also expected to call this routine again
 * @param timeoutS	Timeout between sending the information into the network.
 */
void timer_startInformationDistributionTimer(callbackHandle_t callback, uint16_t timeoutS);

// function declarations only. Functions will be filled in by initialisation routine and take the functions
// defined in the interface structure
/**
 * Base software routine used to schedule a 5ms resolution task. The interface initialization routine takes care of the proper routine location filling
 */
extern scheduleTask_t 		bsw_timer_scheduleTask;

/**
 * Base software routine used ot schedule a 1s resolution task. The interface initialization routine takes care of the proper routine location filling
 */
extern scheduleSlowTask_t 	bsw_timer_scheduleSlowTask;

#endif /* CORE_TIMER_TIMER_H_ */
