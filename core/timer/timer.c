/*
 * timer.c
 *
 *  Created on: 11.10.2021
 *      Author: A.Niggebaum
 */

#include "timer.h"
#include "../../logic_interface.h"

/**
 * Register a callback for the broadcast timer
 *
 * @param callback
 * @param timeoutS
 */
void timer_startBroadcastTimer(callbackHandle_t callback, uint16_t timeoutS)
{
	bsw_timer_scheduleSlowTask(CORE_BASE_TIMER_BROADCAST, callback, timeoutS);
}

void timer_startInformationDistributionTimer(callbackHandle_t callback, uint16_t timeoutS)
{
	bsw_timer_scheduleSlowTask(CORE_BASE_TIMER_INFORMATION_DISTRIBUTION, callback, timeoutS);
}

/**
 * Passthrough for (slow) timer starter
 *
 * @param timerIndex
 * @param timeoutCallback
 * @return
 */
ILB_HAL_StatusTypeDef coreLib_startTimer(uint8_t timerIndex, callbackHandle_t timeoutCallback, uint16_t timeoutMS)
{
	return bsw_timer_scheduleSlowTask(timerIndex + CORE_BASE_TIME_NUMBER_OF, timeoutCallback, timeoutMS);
}
