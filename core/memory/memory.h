/**
 * @file memory.h
 *
 * @date 20.10.2021
 * @author A.Niggebaum
 *
 * @brief Helpers and macros for memory management. Mainly for management of the address table. The size of the table and also the number of persistent variables are defined in the network layout in the logic static library.
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_MEMORY_MEMORY_H_
#define CORE_MEMORY_MEMORY_H_

#include "../../settings/settings.h"

// align macro
#define ALIGN(x,a)				__ALIGN_MASK(x,(typeof(x))(a)-1)	//!< align x to a bit mask a
#define __ALIGN_MASK(x,mask)	(((x)+(mask))&~(mask))				//!< sub macro for align

#define CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS		5				//!< number of bytes per lookup table element

// storage of address table
#define EEPROM_NUMBER_OF_PERIPHERALS_BYTE_POS		0				//!< Positions in eeprom
#define EEPROM_ADDRESS_TABLE_START_POS				1				//!< Address table start position. The first byte contains the number of elments.

// storage of persistent variables. MUST be 32bit aligned
#define EEPROM_PERSISTENT_VARIABLES_START			ALIGN(EEPROM_ADDRESS_TABLE_START_POS + (CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS * SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS), 4)	//!< location of persistent variables start, after the address lookup table.

#endif /* CORE_MEMORY_MEMORY_H_ */
