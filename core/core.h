/**
 * @file core.h
 *
 * @date Sep 27, 2021
 * @author A.Niggebaum
 *
 * @brief Routines for the core state machine. This it what makes the core tick
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_CORE_H_
#define CORE_CORE_H_

#include <stdint.h>
#include "ilb.h"

/**
 * Type and enumeration for errors of the core
 *
 * These errors will cause interruption of the normal operatio and are persistent. They
 * are recoreded in the error memory
 */
typedef enum coreInitialisationStatusEnum{
	CORE_UNINITIALISED							= 0,	//!< default state, the core is not initialised
	CORE_EXECUTE_RESET							= 1,	//!< indicates that the core executes a reset
	CORE_EXECUTE_SETTINGS_RESET					= 2,	//!< indicates that the core executes a settings reset
	CORE_EXECUTE_FACTORY_RESET					= 3,	//!< indicates that the core executes a factory reset
	CORE_WAITING_FOR_PERIPHERAL_UID				= 4,	//!< the core is waiting for the requested peripherals to register with the core or reply to a message
	CORE_CONFIGURING_PERIPHERALS				= 5,	//!< the peripehrals need to be configured according to the logic. This happens after the core has initialised and a network discovery has been performed
	CORE_WAITING_FOR_PERIPHERAL_CONFIG			= 6,	//!< waiting for the configuration of to be applied to the peripheral
	CORE_CHECK_PERIPHERALS						= 7,	//!< the core has restored its state from the eeprom memory and needs to check integrity of the network
	CORE_INITIALISED							= 8,	//!< the core is operating in normal logic conditions
	CORE_INITIALISATION_ERROR					= 9,	//!< an error occured during initialisation
	CORE_INITIALISATION_ERROR_NUMBER_MISMATCH	= 10,	//!< incorrect number of peripherals in the network
	CORE_INITIALISATION_ERROR_WRONG_PERIPHERALS	= 11,	//!< incorrect peripheral types in the network
	CORE_LOGIC_ERROR							= 12,	//!< an error occured during logic operation
	CORE_ERROR_PERIPHERAL_TRACKING				= 13,	//!< an error occured tracking the peripherals. A peripheral was removed or changed
	CORE_ERROR_PERIPHERAL_DOUBLE_ADDRESS		= 14,	//!< two peripherals with the same address were detected in the network.
	CORE_ERROR_PERIPHERAL_MISSING				= 15,	//!< a peripheral that was previously in the network failed to respond and is considered missing. That may be due to failure or because it was disconnected.
	CORE_ERROR_PERIPHERAL_CHECK					= 16,	//!< an error occured while checking the peripherals on the network. This may be a wrong answer, but an answer
	CORE_ERROR_OPEN_CIRCUIT						= 17,	//!< an open circuit was detected
	CORE_ERROR_SHORT_CIRCUIT					= 18,	//!< a short circuit was detected
} CoreStatus_t;

//
//	Public access to interface functions
//
/**
 * Initialize the core. Takes care of filling the location of routiens that are part of the base software as well as initializing all variables in RAM. This is necessary, because no automatic linker script is generated to do this.
 * @param nOfSlowTimers Number of slow timers used by core and logic. Triggers the reservation of sufficient space for the timers
 * @return	::ILB_HAL_OK on success, error code otherwise.
 */
ILB_HAL_StatusTypeDef core_init(uint8_t* nOfSlowTimers);

/**
 * Main core state machine routine. Must be called whenever possible.
 * @return 0x00 if no error occured, error code otherwise.
 */
uint8_t core_logic(void);

//
//	Interface functions
//
/**
 * Set the status of the core. Takes care that the appropriate blink pattern is shown and other eventual tasks, such as the sending of a broadcast, are done.
 * @param status Status to set
 */
void core_setStatus(CoreStatus_t status);

/**
 * Get the current status of the core state machine
 * @return Current core status
 */
CoreStatus_t core_getStatus(void);

#endif /* CORE_CORE_H_ */
