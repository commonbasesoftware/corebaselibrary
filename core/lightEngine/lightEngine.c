/*
 * lightEngine.c
 *
 *  Created on: 05.10.2021
 *      Author: A.Niggebaum
 */

#include "lightEngine.h"
#include "../../settings/settings.h"
#include "../messaging/messaging.h"
#include "../addressing/adressing.h"
#include "../timer/timer.h"
#include "../../logic_interface.h"
#include "../broadcast/broadcast.h"
#include <stddef.h>

#define ATTACHED_LIGHT_ENGINE_RETRY_MS		10
#define ATTACHED_LIGHT_ENGINE_INDEX			0

// interface to logic
extern const NetworkLayoutTypeDef logic_networkLayout;

//
//	Variables for indication via main light engine
//
#define LIGHT_ENGINE_INDICATION_PATTERN 				0b01010101
#define LIGHT_ENGINE_INDICATION_POS_MAX					8
#define LIGHT_ENGINE_INDICATION_PATTERN_SEPARATION_MS	1000
static uint8_t lightEngineIndicationPos	= 0;
static void LightEngineIndicationCallback(void);

//
//	Variables for light engine management and tracking
//
/**
 * Represents a light engine. May be attached or on a peripheral.
 */
typedef struct LightEngineStatusStruct {
	uint8_t  address;				//!< index of address for light engine in address table or marker for attached light engine
	uint8_t  status;				//!< status flags of light engine. May be on/off/fading
	uint16_t primarySidePower;		//!< primary side power consumption (estimated from secondary side) in units of 100mW
	uint16_t secondarySidePower;	//!< secondary side power consumption. Measured by measurement circuit in units of 100mW
	uint8_t failedCommunications;	//!< If the light engine is on a peripheral, the number of failed communication attempts are counted here. A successful communication resets the counter
} LightEngineStatus_t;

static LightEngineStatus_t lightEnginesStatus[SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS];		// reserve enough space for maximal number of peripherals. Malloc because we later compile two libraries into each other. This seems to be a bug of ld

//
//	Private function prototypes
//
void updateLightEngineStatus(uint8_t address);
void sendLevelToPeripheralCallback(ILBMessageStruct* response);
void sendLevelToPeripheralTimout(void);
void checkAttachedLightEngineProgress(void);

//
//	Initialisation routine
//
ILB_HAL_StatusTypeDef lightEngine_initManager(void)
{
	// intialise the structure
	if (logic_networkLayout.numberOfLightEngines > SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS)
	{
		return ILB_HAL_ERROR;
	}
	updateLightEngineStatus(LOGIC_LIGHT_ENGINE_ATTACHED_MARKER);
	for(uint8_t i=1; i<logic_networkLayout.numberOfLightEngines; i++)
	{
		// store address
		lightEnginesStatus[i].address = addressing_getAddressForIndex(logic_networkLayout.lightEngineList[i]);
		lightEnginesStatus[i].failedCommunications = 0;
		// update the list
		updateLightEngineStatus(lightEnginesStatus[i].address);
	}
	return ILB_HAL_OK;
}

//
//	Library interface functions
//
/**
 * Interface function for logic to set light engine output
 *
 * @param LEIndex		index according to logic enum of light engine
 * @param level
 * @param mired
 * @param fadeTimeMS
 */
void coreLib_setLightEngine(uint8_t LEIndex, uint8_t level, uint16_t mired, uint32_t fadeTimeMS, IntensityLookup_t intensityLookup)
{
	if (LEIndex > logic_networkLayout.numberOfLightEngines)
	{
		return;
	}

	// do we need to set the attached light engine?
	if (lightEnginesStatus[LEIndex].address == ATTACHED_LIGHT_ENGINE_INDEX)
	{
		bsw_lightEngine_startFade(level, mired, fadeTimeMS, intensityLookup);
		// update status
		lightEnginesStatus[ATTACHED_LIGHT_ENGINE_INDEX].status |= LIGHT_ENGINE_FADING;
		// publish information
		broadcast_queueBroadcastMessage();
		// track fade
		// check on the progress of the attached light engine
		bsw_timer_scheduleTask(checkAttachedLightEngineProgress, fadeTimeMS + ATTACHED_LIGHT_ENGINE_RETRY_MS);
		return;
	} else {
		// light engine sits on a peripheral. Send out the message
		ILBMessageStruct msg;
		msg.address = lightEnginesStatus[LEIndex].address;
		msg.identifier = ILB_GENERATE_IDENTIFIER;
		msg.messageType = ILB_MESSAGE_COMMAND;
		msg.payloadSize = 6;
		msg.payload[0] = ILB_SET_OUTPUT_CCT;
		msg.payload[1] = ((fadeTimeMS >> 8) & 0xFF);
		msg.payload[2] = (fadeTimeMS & 0xFF);
		msg.payload[3] = level;
		msg.payload[4] = ((mired >> 8) & 0xFF);
		msg.payload[5] = (mired & 0xFF);

		// send message
		bsw_messaging_sendMessage(&msg, sendLevelToPeripheralCallback, SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS, sendLevelToPeripheralTimout);
	}
}

/**
 * Callback sending light level command to driver peripheral
 *
 * @param response
 */
void sendLevelToPeripheralCallback(ILBMessageStruct* response)
{

}

/**
 * Timeout callback sending light level command to driver peripheral
 */
void sendLevelToPeripheralTimout(void)
{

}

/**
 * Set the output of a follower group
 *
 * @param followerGroup
 * @param level
 * @param mired
 * @param fadeTimeMS
 */
void coreLib_setFollower(uint8_t followerGroup, uint8_t level, uint16_t mired, uint16_t fadeTimeMS)
{
	// prepare the message
	ILBMessageStruct followerInstruction;
	followerInstruction.address = ILB_MESSAGE_BROADCAST_ADDRESS;
	followerInstruction.identifier = ILB_GENERATE_IDENTIFIER;
	followerInstruction.messageType = ILB_MESSAGE_COMMAND;
	followerInstruction.payloadSize = 7;
	followerInstruction.payload[0] = ILB_FOLLOWER_INSTRUCTION;
	followerInstruction.payload[1] = followerGroup;
	followerInstruction.payload[2] = ((fadeTimeMS >> 8) & 0xFF);
	followerInstruction.payload[3] = (fadeTimeMS & 0xFF);
	followerInstruction.payload[4] = level;
	followerInstruction.payload[5] = ((mired >> 8) & 0xFF);
	followerInstruction.payload[6] = (mired & 0xFF);

	// send message
	bsw_messaging_sendMessage(&followerInstruction, NULL, 0, NULL);
}

/**
 * Callback when we epxec the attached light engine to have finished the fade
 */
void checkAttachedLightEngineProgress(void)
{
	// get the new status
	uint8_t newStatus = 0;
	uint16_t newPrimarySidePower = 0;
	uint16_t newSecondarySidePower = 0;
	bsw_lightEngine_getStatus(&newStatus, &newPrimarySidePower, &newSecondarySidePower);
	// check if the fade has finished
	if ((newStatus & LIGHT_ENGINE_FADING) == 0)
	{
		// fade has finished
		// push status into structure
		updateLightEngineStatus(LOGIC_LIGHT_ENGINE_ATTACHED_MARKER);
		// notify logic
		logic_attachedLightEngineFadeFinished();
		// trigger broadcast event
		broadcast_queueBroadcastMessage();
	} else {
		// fade is ongoing. Re-trigger
		bsw_timer_scheduleTask(checkAttachedLightEngineProgress, ATTACHED_LIGHT_ENGINE_RETRY_MS);
	}
}

//
//	Routines for save and error indication routines
//

/**
 * Set a save state
 *
 * This routine will be called if an error occured. There is the option to use the attached light engine as an indicate
 */
void lightEngine_setSaveState(uint8_t blinkWithMainLightEngine)
{
	if (blinkWithMainLightEngine != 0)
	{
		// indication is required. Start the process
		// start the pattern
		LightEngineIndicationCallback();
	} else {
		// no indication required. Just set to the save level
		bsw_lightEngine_startFade(LIGHT_ENGINE_MAX_LEVEL, 0, 0, INTENSITY_LOOKUP_LINEAR);
	}
}

/**
 * Callback to use the light engine to indicate errors
 *
 * Steps through the defined pattern and turns the light engine to maximal level if the bit is set.
 * All parameters are defined in the header. After the pattern has been stepped through, the maximal
 * possible level of the light engine is applied. The function will re-trigger again automatically.
 */
void LightEngineIndicationCallback(void)
{
	// decide if to turn on or off the main light engine
	if (((LIGHT_ENGINE_INDICATION_PATTERN >> lightEngineIndicationPos) & 0x01) == 0x01)
	{
		// turn on
		bsw_lightEngine_startFade(LIGHT_ENGINE_MAX_LEVEL, 0, 0, INTENSITY_LOOKUP_LINEAR);
	} else {
		// turn off
		bsw_lightEngine_startFade(0, 0, 0, INTENSITY_LOOKUP_LINEAR);
	}

	// proceed
	lightEngineIndicationPos++;

	// decide if to stop
	if (lightEngineIndicationPos >= LIGHT_ENGINE_INDICATION_POS_MAX)
	{
		// end pattern
		// turn light engine off
		bsw_lightEngine_startFade(LIGHT_ENGINE_MAX_LEVEL, 0, 0, INTENSITY_LOOKUP_LINEAR);
		return;
	}
	// trigger again
	bsw_timer_scheduleTask(LightEngineIndicationCallback, LIGHT_ENGINE_INDICATION_PATTERN_SEPARATION_MS);
}

//
//	Maintenance routines
//
/**
 * Pull the status from the light engine and move it to the list of light engines
 * @param index
 */
void updateLightEngineStatus(uint8_t address)
{
	// case for attached light engine
	if (address == LOGIC_LIGHT_ENGINE_ATTACHED_MARKER)
	{
		bsw_lightEngine_getStatus(&lightEnginesStatus[ATTACHED_LIGHT_ENGINE_INDEX].status, &lightEnginesStatus[ATTACHED_LIGHT_ENGINE_INDEX].primarySidePower, &lightEnginesStatus[ATTACHED_LIGHT_ENGINE_INDEX].secondarySidePower);
		return;
	}

	// the light engine is sitting on a driver peripheral
	// TODO: implement pulling of values (we need status, primary side power and secondary side power)

}

/**
 * Provide the information for the broadcast
 *
 * @param pMemoryStart
 */
void lightEngine_getBroadcastInformation(uint8_t* pMemoryStart)
{
	uint8_t status = 0;
	uint32_t primarySidePower = 0;
	uint32_t secondarySidePower = 0;
	for (uint8_t i=0; i<logic_networkLayout.numberOfLightEngines; i++)
	{
		updateLightEngineStatus(logic_networkLayout.lightEngineList[i]);
		status |= lightEnginesStatus[i].status;
		primarySidePower += lightEnginesStatus[i].primarySidePower;
		secondarySidePower += lightEnginesStatus[i].secondarySidePower;
	}
	pMemoryStart[0] = status;
	pMemoryStart[1] = ((primarySidePower >> 8) & 0xFF);
	pMemoryStart[2] = (primarySidePower & 0xFF);
	pMemoryStart[3] = ((secondarySidePower >> 8) & 0xFF);
	pMemoryStart[4] = (secondarySidePower & 0xFF);
}
