/**
 * @file lightEngine.h
 *
 * @date 05.10.2021
 * @author A.Niggebaum
 *
 * @brief Management functions for the light engines in the network. A core must potentially more than one light engine. The location of the light engines is defined in the network layout. They may be attached to the module or located on peripehrals
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_LIGHTENGINE_LIGHTENGINE_H_
#define CORE_LIGHTENGINE_LIGHTENGINE_H_

#include "ilb.h"

#define LIGHT_ENGINE_MAX_LEVEL		0xFF		//!< maximal light level available on a light engine
#define BLINK_ATTACHED_LIGHT_ENGINE	0x01		//!< Flag if the attached light engine should blink in case of an error.

/**
 * Get the status of the attached light engine. This routine is implemented in the base software static library
 */
extern getLightEngineStatus_t bsw_lightEngine_getStatus;

/**
 * Start a fade of the attached light engine. This routine is implemented in the base software static library.
 */
extern startLightEngineFade_t bsw_lightEngine_startFade;

//
//	Library internal light engine routines
//
/**
 * Initialize the light engine manager. Reserves memory for the number of light engines and their tracking. The number of light engines is defined in the network layout, which is part of the logic static library
 * @return
 */
ILB_HAL_StatusTypeDef lightEngine_initManager(void);

/**
 * Enter a save state, because a critical error has occured.
 * @param blinkWithMainLightEngine If nonzero, the light engine attached to the module will blink in order to visualize the error.
 * NOTE: no light engines on peripehrals will blink
 */
void lightEngine_setSaveState(uint8_t blinkWithMainLightEngine);

//
//	Interface for broadcast messages
//
/**
 * Writes 5 bytes of information into the provided memory location. Use: create a message structure, call this routine pointing at the payload
 * @param pMemoryStart Start of memory location where to place the information. 5 bytes are written.
 */
void lightEngine_getBroadcastInformation(uint8_t* pMemoryStart);	// writes 5 bytes starting at the provided location containing the broadcast information

#endif /* CORE_LIGHTENGINE_LIGHTENGINE_H_ */
