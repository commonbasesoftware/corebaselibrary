/**
 * @file persistentVariables.h
 *
 * @date 20.10.2021
 * @author A.Niggebaum
 *
 * @brief Management of persistent variables
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_PERSISTENTVARIABLES_PERSISTENTVARIABLES_H_
#define CORE_PERSISTENTVARIABLES_PERSISTENTVARIABLES_H_

#include "ilb.h"

/**
 * Initializes the persistent variables management. Loads variables from the memory if found
 * @return ::ILB_HAL_OK on success, error code otherwise.
 */
ILB_HAL_StatusTypeDef persistentVariables_init(void);

#endif /* CORE_PERSISTENTVARIABLES_PERSISTENTVARIABLES_H_ */
