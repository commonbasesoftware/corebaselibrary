/*
 * persistentVariables.c
 *
 *  Created on: 20.10.2021
 *      Author: A.Niggebaum
 */

#include "ilb.h"
#include "persistentVariables.h"
#include "../../logic_interface.h"
#include "../memory/memory.h"
#include "../interface/interface.h"

// get the structure from the logic library
extern const logicPersistentVariables_t logic_nonVolatileVariables;

static uint32_t persistentVariables[SETTING_MAX_NOF_PERSISTENT_LOGIC_VARS] = {0};		//!! malloc does throw error when compiled with other library

/**
 * Initialise the persistent variables
 *
 * @return
 */
ILB_HAL_StatusTypeDef persistentVariables_init(void)
{
	if (logic_nonVolatileVariables.nOfPersistentVariables > SETTING_MAX_NOF_PERSISTENT_LOGIC_VARS)
	{
		return ILB_HAL_ERROR;
	}
	ILB_HAL_StatusTypeDef success = ILB_HAL_OK;
	// read from memory
	for (uint8_t i=0; i<logic_nonVolatileVariables.nOfPersistentVariables; i++)
	{
		success |= bsw_EEPROMInterface(EEPROM_VAR_WORD, EEPROM_PERSISTENT_VARIABLES_START + i * 4, &persistentVariables[i]);
	}
	// signal outcome
	return success;
}

/**
 * Reset the variables to defaul values
 */
void persistentVariables_reset(void)
{
	for (uint8_t i=0; i<logic_nonVolatileVariables.nOfPersistentVariables; i++)
	{
		persistentVariables[i] = logic_nonVolatileVariables.listOfDefaults[i];
	}
}

/**
 * Set a variable to a new value
 *
 * @param index
 * @param value
 * @return
 */
ILB_HAL_StatusTypeDef persistentVariables_set(uint8_t index, uint32_t value)
{
	if (index < logic_nonVolatileVariables.nOfPersistentVariables)
	{
		persistentVariables[index] = value;
		return ILB_HAL_OK;
	}
	return ILB_HAL_ERROR;
}

/**
 * Get the value of a variable
 *
 * @param index
 * @param var
 * @return
 */
ILB_HAL_StatusTypeDef persistentVariables_get(uint8_t index, uint32_t* var)
{
	if (index < logic_nonVolatileVariables.nOfPersistentVariables)
	{
		*var = persistentVariables[index];
		return ILB_HAL_OK;
	}
	return ILB_HAL_ERROR;
}

/**
 * Write the variables to memory
 * @return
 */
ILB_HAL_StatusTypeDef persistentVariables_writeToMemory(void)
{
	ILB_HAL_StatusTypeDef success = ILB_HAL_OK;
	for (uint8_t i=0; i<logic_nonVolatileVariables.nOfPersistentVariables; i++)
	{
		success |= bsw_EEPROMInterface(EEPROM_VAR_WORD | EEPROM_VAR_WRITE, EEPROM_PERSISTENT_VARIABLES_START + i * 4, &persistentVariables[i]);
	}
	return success;
}
