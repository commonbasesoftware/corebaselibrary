/**
 * @file register.h
 *
 * @date 05.10.2021
 * @author A.Niggebaum
 *
 * @brief Helper functions to push status information to the register accesible via the ILB interface
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_REGISTER_REGISTER_H_
#define CORE_REGISTER_REGISTER_H_

#include "../core.h"

/**
 * Set the system status register
 * @param status Current status of the system
 */
void register_core_setSystemStatus(CoreStatus_t status);

/**
 * Set the error register
 * @param error Any current error flags.
 */
void register_core_setSystemFlag(SystemErrorFlag_t error);

#endif /* CORE_REGISTER_REGISTER_H_ */
