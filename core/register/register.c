/*
 * register.c
 *
 *  Created on: 05.10.2021
 *      Author: A.Niggebaum
 */

#include "../core.h"

/**
 * Set the byte representing the status of the core
 *
 * @param status
 */
void register_core_setSystemStatus(CoreStatus_t status)
{

}

/**
 * Set the error flag
 *
 * @param error
 */
void register_core_setSystemFlag(SystemErrorFlag_t error)
{

}
