/*
 * broadcast.c
 *
 *  Created on: 11.10.2021
 *      Author: A.Niggebaum
 */

#include <stddef.h>
#include "ilb.h"
#include "../timer/timer.h"
#include "../messaging/messaging.h"
#include "../../settings/settings.h"
#include "../lightEngine/lightEngine.h"
#include "../core.h"

// define to check which core state is considered an error state (will set the system error flag). Each bit position encodes a state in the CoreStatus_t enum
#define CORE_ERROR_STATES_DEF		0b1111111111000000000

typedef enum BroadcastStatus {
	BROADCAST_DISABLED,
	BROADCAST_WAITING,
	BROADCAST_ARMED,
} BroadcastStatus_t;
static BroadcastStatus_t broadcastActive = BROADCAST_DISABLED;

//
//	Function prototypes
//
void armBroadcast(void);

/**
 * Start periodic broadcast messagses
 */
void broadcast_startBroadcastMessages(void)
{
	// set status
	broadcastActive = BROADCAST_WAITING;
	// trigger timer
	timer_startBroadcastTimer(armBroadcast, SETTINGS_BROADCAST_INTERVAL_S);
}

/**
 * Disable broadcast messages
 */
void broadcast_stopBroadcastMessages(void)
{
	broadcastActive = BROADCAST_ARMED;
}

/**
 * Handle broadcast messages that may need to be send
 */
void broadcast_handle(void)
{
	if (broadcastActive == BROADCAST_ARMED)
	{
		// send out the message
		ILBMessageStruct broadcastMessage;
		broadcastMessage.address = ILB_MESSAGE_BROADCAST_ADDRESS;
		broadcastMessage.identifier = ILB_GENERATE_IDENTIFIER;
		broadcastMessage.messageType = ILB_MESSAGE_COMMAND;
		broadcastMessage.payloadSize = 6;
		broadcastMessage.payload[0] = ILB_BROADCAST_INFO;
		// get information from light engines
		lightEngine_getBroadcastInformation(&broadcastMessage.payload[1]);

		// check if the core is in an error state
		CoreStatus_t coreStatus = core_getStatus();
		if (((0x01 << coreStatus) & CORE_ERROR_STATES_DEF) != 0)
		{
			// core is in an error state. Set the system error flag
			broadcastMessage.payload[1] |= BROADCAST_FLAG_SYSTEM_FAILURE;
		}

		// send
		bsw_messaging_sendMessage(&broadcastMessage, NULL, 0, NULL);

		// re-trigger broadcast
		timer_startBroadcastTimer(armBroadcast, SETTINGS_BROADCAST_INTERVAL_S);

		// send to waiting
		broadcastActive = BROADCAST_WAITING;
	}
}

/**
 * Arm the broadcast system to send a message at the next possible time
 *
 * To be called from logic
 */
void broadcast_queueBroadcastMessage(void)
{
	broadcastActive = BROADCAST_ARMED;
}

/**
 * Callback to arm the broadcast via a timer
 */
void armBroadcast(void)
{
	if (broadcastActive != BROADCAST_DISABLED)
	{
		broadcastActive = BROADCAST_ARMED;
	}
}
