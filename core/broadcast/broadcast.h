/**
 * @file broadcast.h
 *
 * @date 11.10.2021
 * @author Author: A.Niggebaum
 *
 * @brief Broadcast functionality of the core
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_BROADCAST_BROADCAST_H_
#define CORE_BROADCAST_BROADCAST_H_

/**
 * Start the periodic emission of broadcast messages. Once started, a broadcast message would be queued for sending in regular intervals
 */
void broadcast_startBroadcastMessages(void);

/**
 * Stop the periodic emission of broadcast messages.
 */
void broadcast_stopBroadcastMessages(void);

/**
 * Place a broadcast message in the sending queue. Use this routine to emitt a broadcast out of the normal interval. This is done when e.g. a light engine fade has finished
 */
void broadcast_queueBroadcastMessage(void);

/**
 * Handle pending broadcast message flags. This routine should be called as often as possible. The other routines merely arm the broadcast mechanism. This routine
 * checks if the mechanism is armed and writes an up to date broadcast message to the outgoing message buffer. This reduces the risk of outdated broadcast information.
 */
void broadcast_handle(void);

#endif /* CORE_BROADCAST_BROADCAST_H_ */
