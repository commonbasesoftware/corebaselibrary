/*
 * interface.c
 *
 *  Created on: Sep 27, 2021
 *      Author: A.Niggebaum
 */

#include "ilb.h"
#include "ilbstd.h"
#include "../core.h"
#include "../messaging/messaging.h"

const __interfaceLogic CoreLogicInterface_t coreLogicInterface = {
		2,											// version
		{0x1D, 0x5E, 0xFF, 0xFF, 0xFF, 0xFF},		// test gtin
		0,											// test major version
		1,											// test minor version
		core_init,									// initialisation function
		core_logic,									// logic routine function
		core_commandHandler,						// command handler
		core_readHandler,							// read handler
		core_writeHandler							// write handler
};

//
//	Old version
//
/*

Changes:
1: Introduced telemetry interface in BaseSoftwareInterface_t

const __interfaceLogic CoreLogicInterface_t coreLogicInterface = {
		1,											// version
		{0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF},		// test gtin
		0,											// test major version
		1,											// test minor version
		core_init,									// initialisation function
		core_logic,									// logic routine function
		core_commandHandler,						// command handler
		core_readHandler,							// read handler
		core_writeHandler							// write handler
};

 */
