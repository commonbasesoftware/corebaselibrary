/**
 * @file interface.h
 *
 * @date 28.09.2021
 * @author A.Niggebaum
 *
 * @brief Routines of the base software (bsw) accessible through the interface. All routines are marked as extern and are expected to be implemented in compiled static library of the base software.
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_INTERFACE_INTERFACE_H_
#define CORE_INTERFACE_INTERFACE_H_

#include <stdint.h>
#include "ilb.h"

// functions provided by the base software
/**
 * Get the number of ticks (ms) since the processor has started
 */
extern getTick_t  bsw_getTick;

/**
 * Delay execution by a set amount of ms
 */
extern delay_t	  bsw_delay;

/**
 * Get a random number. It depends on the hardware if this is a pseudo random number or the result of true random number generator
 */
extern getRandomNumber_t bsw_getRandomNumber;

/**
 * Get the interface to the EEPROM to access stored information or store values in the persistent memory
 */
extern eepromInterface_t bsw_EEPROMInterface;

/**
 * Get the interface to the telemetry.
 */
extern telemetryInterface_t bsw_telemetryInterface;

#endif /* CORE_INTERFACE_INTERFACE_H_ */
