/**
 * @file network.h
 *
 * @date Sep 27, 2021
 * @author A.Niggebaum
 *
 * @brief Network supervision routines to confirm pre-configured networks and monitor networks for disappearing peripherals.
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_NETWORK_NETWORK_H_
#define CORE_NETWORK_NETWORK_H_

#include "ilb.h"

/**
 * Starts a routine to confirm the UID of peripherals. Called after booting and encountering a non-empty address table. The routine is NON-BLOCKING and will iterate through the peripherals in the address table and read out their UID, confirming the individual. If a discrepancy is detected, it aborts with an error. The first call starts the process. The Return values are interpreted as:
 * - ::ILB_HAL_OK: routine has terminated with success
 * - ::ILB_HAL_BUSY: routine is busy, probe again.
 * - any other error code: The routine has aborted with an error. Peripherals are missing or the UID does not match.
 * @return Status of the checking routine
 */
ILB_HAL_StatusTypeDef networkCheck_confirmUIDOfPeripherals(void);

/**
 * Starts a periodic network check that will read out the tracker byte of the peripherals in the network to monitor if they have lost power during operation.
 */
void networkCheck_startPeriodicNetworkCheck(void);

/**
 * Starts the information distribution mechanism.
 *
 * Checks if the network contains devices that require an update about certain pices of information available in the core.
 */
void informationDistribution_start(void);

#endif /* CORE_NETWORK_NETWORK_H_ */
