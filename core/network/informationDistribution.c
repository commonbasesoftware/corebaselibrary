/*
 * informationDistribution.c
 *
 *  Created on: 13.01.2022
 *      Author: A.Niggebaum
 *
 * The information distribution mechanism refers to the mechanism to distribute information available in the core onto
 * devices in the network.
 *
 * For example the measured driver temperature may be published to peripherals that bridge to other networks such as DALI or ZigBee.
 *
 * Multiple restrictions apply to facilitate the distribution.
 * 1) Only interface peripherals, meaning peripherals that bridge to a DALI or ZigBee network, are receivers of information pieces.
 * 2) There is maximally exactly one DALI peripheral OR exactly one ZigBee peripheral.
 * Automatic distribution to other peripherals, such as driver or sensor, is currently out of scope.
 *
 * This means
 * - There is only one set of information and one target for the information.
 * - The set of information and the target must be selected depending on the network layout
 *
 * Failure mechanisms:
 * The information distribution mechanism will fail silently. This means
 * - The first detected information target will be used. (this assumes there can be only one target. See above)
 *   - If multiple targets are present, all following will be ignored
 *
 * OPEN: how to react to no response or failed write response. Currently no action is taken. Network integrity is currently monitored
 * 		 via the tracker byte. Hence using this information seems not necessary.
 *
 * !! IMPORTANT !!
 * Each routine must check independently if the core operates correctly and pause execution if necessary
 */

#include <stdint.h>
#include <stddef.h>
#include "../core.h"
#include "../network/network.h"
#include "../../settings/settings.h"
#include "../timer/timer.h"
#include "../../logic_interface.h"
#include "../messaging/messaging.h"
#include "../addressing/adressing.h"
#include "../interface/interface.h"

#define MECHANISM_DISABLED_MARKER		0xFF

// Private function prototypes
void informationDistribution_ZigBeeCallback(void);
void informationDistribution_DALICallback(void);
void informationDistribution_responseCallback(ILBMessageStruct* m);
void informationDistribution_responseTimout(void);

// link to network definition
extern const NetworkLayoutTypeDef logic_networkLayout;

//
//	Private variables
//
static uint8_t targetAddress = MECHANISM_DISABLED_MARKER;
static ILBMessageStruct reportMessage;

//
//	Definition of information target
//
//	We assume that there is always exactly one target of each type. E.g. there can only be a single ZigBee peripheral in the network
//  and a single DALI peripheral in the network.
//


void informationDistribution_start(void)
{
	for (uint8_t i=0; i<logic_networkLayout.numberOfGroups; i++)
	{
		// Check the network layout for supported device types
		switch (logic_networkLayout.groups[i]->groupID)
		{
		case ILB_PERIPHERAL_ZIGBEE_CLASS:
			targetAddress = addressing_getAddressForIndex(*logic_networkLayout.groups[i]->addressIndexList);
			timer_startInformationDistributionTimer(informationDistribution_ZigBeeCallback, SETTINGS_INFORMATION_DISTRIBUTION_INTERVAL_S);
			return;

		case ILB_PERIPHERAL_DALI_CLASS:
			targetAddress = addressing_getAddressForIndex(*logic_networkLayout.groups[i]->addressIndexList);
			timer_startInformationDistributionTimer(informationDistribution_DALICallback, SETTINGS_INFORMATION_DISTRIBUTION_INTERVAL_S);
			return;
		}
	}
}

/**
 * Routine to periodically push all ZigBee relevant data to the ZigBee peripheral
 */
void informationDistribution_ZigBeeCallback(void)
{
	// re-trigger routine. This is done at the beginning to save space
	timer_startInformationDistributionTimer(informationDistribution_ZigBeeCallback, SETTINGS_INFORMATION_DISTRIBUTION_INTERVAL_S);

	// ensure that we are in normal operation state
	if (core_getStatus() != CORE_INITIALISED)
	{
		// abort. The core is not operating normally. All telemetry reporting is suspended until we resume normal operation
		return;
	}

	// prepare message
	reportMessage.address = targetAddress;
	reportMessage.identifier = ILB_GENERATE_IDENTIFIER;
	reportMessage.messageType = ILB_MESSAGE_WRITE;
	reportMessage.payloadSize = 3;
	reportMessage.payload[0] = 0x04;
	reportMessage.payload[1] = 0x01;

	// place temperature value
	bsw_telemetryInterface(&reportMessage.payload[2], TELEMETRY_DRIVER_TEMPERATURE);

	// send message
	bsw_messaging_sendMessage(&reportMessage, informationDistribution_responseCallback, SETTING_NETWORK_APPLY_SETTING_TIMEOUT_MS, informationDistribution_responseTimout);
}

/**
 * Routine to execute information distribution to a DALI peripheral
 */
void informationDistribution_DALICallback(void)
{
	// re-trigger routine. This is done at the beginning to save space
	timer_startInformationDistributionTimer(informationDistribution_DALICallback, SETTINGS_INFORMATION_DISTRIBUTION_INTERVAL_S);

	// ensure that we are in normal operation state
	if (core_getStatus() != CORE_INITIALISED)
	{
		// abort. The core is not operating normally. All telemetry reporting is suspended until we resume normal operation
		return;
	}
}

/**
 * Callback to a response to write telemetry information to a peripheral.
 *
 * @param m
 */
void informationDistribution_responseCallback(ILBMessageStruct* m)
{

}

/**
 * Callback if no response to writing telemetry data was recorded.
 */
void informationDistribution_responseTimout(void)
{

}
