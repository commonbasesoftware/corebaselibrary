/*
 * check.c
 *
 *  Created on: Sep 27, 2021
 *      Author: A.Niggebaum
 */

#include "ilb.h"
#include "../../logic_interface.h"
#include "../messaging/messaging.h"
#include "../addressing/adressing.h"
#include "../timer/timer.h"
#include "../core.h"
#include "../../settings/settings.h"
#include "../broadcast/broadcast.h"

//
//	Settings for procedure
//
#define TRACKER_CHECK_TIMEOUT_MS		500

#define CORE_NETWORK_CHECK_BYTES_TO_READ			2										//!< number of bytes to read. !!! also used in networkCheckMessageCallback() to check received answer length. However if only one byte is read, two bytes (0x00 padding) are returned on read
#define CORE_NETWORK_CHECK_PRIM_ADDR				ILB_PERIPHERAL_STATUS_BASE_ADDRESS		//!< primary address to read
#define CORE_NETWORK_CHECK_SEC_ADDR					ILB_TRACKER_SUB_ADDRESS					//!< secondary address to read
#define CORE_NETWORK_CHECK_TRACKER_INDEX			0										//!< index of expected tracker byte
#define CORE_NETWORK_CHECK_STATUS_INDEX				1										//!< index of expected status byte
#define CORE_NETWORK_CHECK_MAJOR_ERROR_NIBBLE		0x03									//!< if any bit is set in this area, the core will enter an error state
#define CORE_NETWORK_CHECK_LOGIC_ERROR_NIBBLE		0xFC									//!< any bit in this area is communicated to the logic


//
//	Interface to logic
//
extern const NetworkLayoutTypeDef logic_networkLayout;

//
//	Private variables
//

/**
 * Type and enumeration for warnings that occur during operation
 *
 * These warnings are considered worthy to be recorded in the error memory. After processing the error, the
 * error state will be reset and normal operation continues
 */
typedef enum {
	CORE_WARNING_NONE,															//!< no warning pending
	CORE_WARNING_TRACKER_LOST,													//!< indicates that the volatile tracker variable was lost in a peripheral. This happens if the peripheral looses power temporarily. The system can recover from this state
	CORE_WARNING_DOUBLE_ADDRESSES_DETECTED,										//!< warning that a double address was detected. If an error state is set, depends if the system can recover or not
	CORE_WARNING_MEMORY_WRITE_ERROR
} CoreWarning_t;

typedef enum coreTrackerCheckStatusEnum {
	CORE_TRACKER_CHECK_STATUS_WAITING,							//!< indicates that a tracker read query was sent and we are waiting for the reply
	CORE_TRACKER_CHECK_APPLYING,								//!< indicates that the write message has been sent and we are waiting for the reply of ILB_OK or nothing
	CORE_TRACKER_CHECK_STATUS_READY,							//!< indicates that the tracker read query was sucessfully handled
	CORE_TRACKER_CHECK_STATUS_ERROR,							//!< general error indicator
	CORE_TRACKER_CHECK_STATUS_PERIPHERAL_MISSING				//!< peripheral is found to be missing
} CoreTrackerStatus_t;
static CoreTrackerStatus_t coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_READY;		//!< tracker variable for status of tracker checking routine
static CoreWarning_t coreTrackingWarningResult = CORE_WARNING_NONE;
static uint8_t nOfCheckedPeripherals = 0;
static uint8_t peripheralTrackerReadAttempts = 0;

//
//	Private function prototypes
//
void handlePeripheralCheckCallback(ILBMessageStruct* message);
void handlePeripheralCheckTimeout(void);
void trackerUpdateCallback(ILBMessageStruct* message);
void trackerUpdateTimeoutCallback(void);

void networkCheck_executePeriodicCheck(void);
void networkCheckMessageCallback(ILBMessageStruct* message);
void networkCheckTimeoutCallback(void);
void trackerUpdateCallback(ILBMessageStruct* message);
void trackerUpdateTimeoutCallback(void);

/**
 * Confirm the UID of the peripherals in the address table
 *
 * This function is typically called after the system reboots and needs to check if the same peripherals as before are present.
 * This function is re-entrant and periodically called from the main logic. Depending on the return value, it will decide if to
 * progress a step or try again.
 *
 * @return ILB_HAL_OK if the process has terminted, ILB_HAL_BUSY, when the process is ongoing, ILB_HAL_ERROR in case of an error. ILB_HAL_INVALID is returned if a discrepancy between stored and found peripherals is encountered
 */
ILB_HAL_StatusTypeDef networkCheck_confirmUIDOfPeripherals(void)
{
	// check if an error occured
	if (coreTrackerStatus == CORE_TRACKER_CHECK_STATUS_ERROR)
	{
		return ILB_HAL_ERROR;
	}

	if (coreTrackerStatus == CORE_TRACKER_CHECK_STATUS_PERIPHERAL_MISSING)
	{
		return ILB_HAL_INVALID;
	}

	// check if we are waiting for an answer or can progress
	if (coreTrackerStatus == CORE_TRACKER_CHECK_STATUS_READY)
	{
		if (nOfCheckedPeripherals == logic_networkLayout.totalNumberOfPeripherals)
		{
			// we checked all of them. All good
			return ILB_HAL_OK;
		}
		// we need to check the next one
		ILBMessageStruct query;
		query.address = addressing_getAddressForIndex(nOfCheckedPeripherals);
		query.identifier = ILB_GENERATE_IDENTIFIER;
		query.messageType = ILB_MESSAGE_READ;
		query.payloadSize = 3;
		query.payload[0] = ILB_PERIPHERAL_INFO_BASE_ADDRESS;
		query.payload[1] = ILB_UID_BASE_ADDRESS;
		query.payload[2] = 4;

		// send and set status
		bsw_messaging_sendMessage(&query, handlePeripheralCheckCallback, TRACKER_CHECK_TIMEOUT_MS, handlePeripheralCheckTimeout);
		coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_WAITING;
		return ILB_HAL_BUSY;
	}
	// we are waiting for an answer. Timeout is handled by separate callback
	return ILB_HAL_BUSY;
}

/**
 * Callback to handle UID response of peripheral after load from eeprom
 *
 * This function will check the response message payload against the expected UID. If the UID matches the expected values in the table, the status
 * variable ::coreTrackerStatus is set that coreCheckPeripherals() sends the request to the next peripheral or decides that all are good on the next
 * iteration.
 *
 * \param message Pointer to ::ILBMessageStruct containing the reply message
 */
void handlePeripheralCheckCallback(ILBMessageStruct* message)
{
	// check the payload length
	if (message->payloadSize != 4) {
		// payload length does not agree. This means the peripheral sent a wrong answer. This could be
		// because it is not the correct peripheral or because of another error
		coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_ERROR;
		return;
	}

	// we have the correct payload length. Check against entry
	uint32_t receivedUID = 0;
	for (uint8_t i=0; i<4; i++) {
		receivedUID |= (message->payload[i] << ((3-i)*8));
	}
	if (addressing_getAddressTableEntry(nOfCheckedPeripherals)->UID != receivedUID) {
		// error
		coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_PERIPHERAL_MISSING;
		return;
	}
	// all good. Write new tracker variable to peripheral
	ILBMessageStruct check;
	check.address = addressing_getAddressTableEntry(nOfCheckedPeripherals)->address;
	check.identifier = ILB_GENERATE_IDENTIFIER;
	check.messageType = ILB_MESSAGE_WRITE;
	check.payloadSize = 3;
	check.payload[0] = ILB_PERIPHERAL_STATUS_BASE_ADDRESS;
	check.payload[1] = ILB_TRACKER_SUB_ADDRESS;
	check.payload[2] = addressing_assingTrackerValueToAddress(nOfCheckedPeripherals);

	bsw_messaging_sendMessage(&check, trackerUpdateCallback, TRACKER_CHECK_TIMEOUT_MS, trackerUpdateTimeoutCallback);

	// set status that we are applying the new tracker
	coreTrackerStatus = CORE_TRACKER_CHECK_APPLYING;
}

/**
 * Callback function upon querying for the UID of a peripheral
 *
 * This function is executed if the reading of a UID times out. This means that the peripheral is
 * - no longer in the network
 * - does not reply because it is powered own or broken
 * Either way, it is considered missing and the satus of the core is set accordingly
 */
void handlePeripheralCheckTimeout(void)
{
	// indicate that the peripheral is missing
	coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_PERIPHERAL_MISSING;
}

/**
 * Callback for tracker update write message
 */
void trackerUpdateCallback(ILBMessageStruct* message)
{
	// check for ILB_OK message
	if (message->payloadSize == 1 && message->payload[0] == ILB_OK) {
		// all good, proceed to next
		nOfCheckedPeripherals++;
		coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_READY;
		return;
	} else {
		// something went wrong updating the tracker. Mark as error
		core_setStatus(CORE_ERROR_PERIPHERAL_TRACKING);
	}
}

/**
 * Callback for timed out tracker update message
 *
 * This function is called if the peripheral fails to respond to the tracker reading.
 *
 */
void trackerUpdateTimeoutCallback(void)
{
	// the update process did timeout. We could not write the new tracker value to the peripehral
	// enter error state
	core_setStatus(CORE_ERROR_PERIPHERAL_TRACKING);
}

//
//	Section: Period network check
//
/**
 * Start the periodic network check
 *
 * This will trigger a sequence that iterates through the peripherals and reads their tracker byte and compares it with the stored byte
 */
void networkCheck_startPeriodicNetworkCheck(void)
{
	if (logic_networkLayout.totalNumberOfPeripherals > 0) {
		nOfCheckedPeripherals = 0;
		bsw_timer_scheduleTask(networkCheck_executePeriodicCheck, SETTING_PERIODIC_NETWORK_CHECK_INTERVAL_MS);
	}
}

/**
 * Starts a network check.
 *
 * This function starts a network check that will:
 * - check the presence of all required peripherals in the network
 * - check the configuration of all individuals in the network
 * This is done via a tracker variable that is once set and read again. If the peripheral power cycles, the variable
 * is reset, because it is not stored persistently. If the peripheral is still reachable via the (randomized) same address,
 * it can be assumed that the configuration was not changed since to change it, it would need to be integrated into another network
 * which will cause a new assignment of an address.
 */
void networkCheck_executePeriodicCheck(void)
{
	// this callback might be called when the core has entered an error state but should be executed
	if (core_getStatus() != CORE_INITIALISED) {
		return;
	}
	// reset the tracker variable
	nOfCheckedPeripherals = 0;
	coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_WAITING;
	coreTrackingWarningResult = CORE_WARNING_NONE;

	ILBMessageStruct query;
	query.address = addressing_getAddressTableEntry(nOfCheckedPeripherals)->address;
	query.identifier = ILB_GENERATE_IDENTIFIER;
	query.messageType = ILB_MESSAGE_READ;
	query.payloadSize = 3;
	query.payload[0] = ILB_PERIPHERAL_STATUS_BASE_ADDRESS;
	query.payload[1] = ILB_TRACKER_SUB_ADDRESS;
	query.payload[2] = 1;

	bsw_messaging_sendMessage(&query, networkCheckMessageCallback, SETTING_NETWORK_CHECK_TIMEOUT_MS, networkCheckTimeoutCallback);
}

/**
 * Network checker message feedback callback
 *
 * This function is called upon receival of a reply message to the queries started in the startNetworkCheck() function. It checks the
 * integrity (correct content and format) of the received message and marks the tracker cycle to be ready to have received a reply
 * to a query (see ::coreTrackerStatus variable).
 *
 * @param message Pointer to message that was received as an answer to the tracker read request.
 */
void networkCheckMessageCallback(ILBMessageStruct* message)
{
	// check if the answer is an error reply or contains bytes
	if (message->messageType != ILB_MESSAGE_ACHKNOWLEDGE) {
		// set marker for incorrect tracker reply
		nOfCheckedPeripherals = logic_networkLayout.totalNumberOfPeripherals + 1;
	} else if (message->payloadSize != CORE_NETWORK_CHECK_BYTES_TO_READ) {
		// set marker for incorrect tracker reply
		nOfCheckedPeripherals = logic_networkLayout.totalNumberOfPeripherals + 1;
	} else {
		//
		//	Handle the tracker
		//
		uint8_t receivedTracker = (message->payload[CORE_NETWORK_CHECK_TRACKER_INDEX]);
		// check if the received bytes correspond to the last transmitted tracker
		if (receivedTracker == addressing_getAddressTableEntry(nOfCheckedPeripherals)->tracker) {
			// count up index
			nOfCheckedPeripherals++;
			// clear the tracker variable. Indicate that a message has been received
			coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_READY;
		} else if (coreTrackerStatus == CORE_TRACKER_CHECK_STATUS_READY) {
			// we have already received a reply. If we received this second one, we have two times the same address in the network
			coreTrackingWarningResult = CORE_WARNING_DOUBLE_ADDRESSES_DETECTED;
			// indicate general error to networkCheckTimeoutCallback()
			nOfCheckedPeripherals = logic_networkLayout.totalNumberOfPeripherals + 1;
		} else {
			// set marker for incorrect tracker reply
			// this will cause a re-writing of the tracker byte once the ::networkCheckTimeoutCallback is triggered
			coreTrackingWarningResult = CORE_WARNING_TRACKER_LOST;
			nOfCheckedPeripherals = logic_networkLayout.totalNumberOfPeripherals + message->address;
		}
		//
		//	Handle the status
		//
		uint8_t receivedPeripheralStatus = (message->payload[CORE_NETWORK_CHECK_STATUS_INDEX]);
		if ((receivedPeripheralStatus & CORE_NETWORK_CHECK_MAJOR_ERROR_NIBBLE) != 0x00) {
			core_setStatus(CORE_ERROR_PERIPHERAL_TRACKING);
		} else if ((receivedPeripheralStatus & CORE_NETWORK_CHECK_LOGIC_ERROR_NIBBLE) != 0x00) {
			// push the error code to the logic
			//peripheralErrorCodeRecieved(receivedPeripheralStatus, message->address);
		}
	}
	// register this function again to check double replies (double addresses) and trigger the timeout callback after a time
	message->messageType = ILB_MESSAGE_HANDLER_ONLY;
	bsw_messaging_sendMessage(message, networkCheckMessageCallback, SETTING_NETWORK_CHECK_TIMEOUT_MS, networkCheckTimeoutCallback);
}

/**
 * Network checker timeout callback
 *
 * This function is called at the end of the network check timeout that is given the peripherals to respond. It will check if only one
 * peripheral has responded (expected case) and send the tracker query message to the next peripehral in the list.
 * In case no peripheral has answered, it is assumed that the peripheral is missing from the network. If, within the time of the
 * timeout before networkChecktimeoutCallback() is called, two peripherals respond to the same address, it is assumed that a second
 * peripheral with the same address was attached and that the network is in an address collision state.
 */
void networkCheckTimeoutCallback(void)
{
	// check if a timeout occured while trying to read the tracker variable
	if (coreTrackerStatus == CORE_TRACKER_CHECK_STATUS_WAITING && coreTrackingWarningResult == CORE_WARNING_NONE) {
		// we did not receive any reply. The peripheral is no longer in the network
		// try again
		if (peripheralTrackerReadAttempts < SETTING_NETWORK_CHECK_TRACKER_READ_ATTEMPTS) {
			// try again
			ILBMessageStruct query;
			query.address = addressing_getAddressForPeripheral(nOfCheckedPeripherals);
			query.identifier = ILB_GENERATE_IDENTIFIER;
			query.messageType = ILB_MESSAGE_READ;
			query.payloadSize = 3;
			query.payload[0] = CORE_NETWORK_CHECK_PRIM_ADDR;
			query.payload[1] = CORE_NETWORK_CHECK_SEC_ADDR;
			query.payload[2] = CORE_NETWORK_CHECK_BYTES_TO_READ;

			bsw_messaging_sendMessage(&query, networkCheckMessageCallback, SETTING_NETWORK_CHECK_TIMEOUT_MS, networkCheckTimeoutCallback);

			// set tracker status
			coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_WAITING;
			// increase tracker counter
			peripheralTrackerReadAttempts++;
		} else {
			// set the status
			core_setStatus(CORE_ERROR_PERIPHERAL_MISSING);
			// update the network
			broadcast_queueBroadcastMessage();
		}
		return;
	}

	// check if we received a wrong tracker or shall proceed to next query
	if (nOfCheckedPeripherals < logic_networkLayout.totalNumberOfPeripherals) {
		// send the request to the next peripheral
		peripheralTrackerReadAttempts = 0;

		ILBMessageStruct query;
		query.address = addressing_getAddressForPeripheral(nOfCheckedPeripherals);
		query.identifier = ILB_GENERATE_IDENTIFIER;
		query.messageType = ILB_MESSAGE_READ;
		query.payloadSize = 3;
		query.payload[0] = CORE_NETWORK_CHECK_PRIM_ADDR;
		query.payload[1] = CORE_NETWORK_CHECK_SEC_ADDR;
		query.payload[2] = CORE_NETWORK_CHECK_BYTES_TO_READ;

		bsw_messaging_sendMessage(&query, networkCheckMessageCallback, SETTING_NETWORK_CHECK_TIMEOUT_MS, networkCheckTimeoutCallback);
		// set tracker status
		coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_WAITING;

	} else if (nOfCheckedPeripherals == logic_networkLayout.totalNumberOfPeripherals) {
		// all good and we went through the complete list
		peripheralTrackerReadAttempts = 0;
		// start the process again after the timeout
		bsw_timer_scheduleTask(networkCheck_executePeriodicCheck, SETTING_PERIODIC_NETWORK_CHECK_INTERVAL_MS);
		// reset tracker status
		coreTrackerStatus = CORE_TRACKER_CHECK_STATUS_READY;
	} else {
		// check what exactly has happened
		if (coreTrackingWarningResult == CORE_WARNING_DOUBLE_ADDRESSES_DETECTED) {
			// this is classified as an error
			core_setStatus(CORE_ERROR_PERIPHERAL_DOUBLE_ADDRESS);
		} else if (coreTrackingWarningResult == CORE_WARNING_TRACKER_LOST) {
			// issue the warning, that a tracker was lost. This might happen due to power loss. To verify that the same
			// Peripheral is attached, we need to check the stored UID in coreAddressLookupTable, against the UID of
			// the device. If address and UID match, it is unlikely that configuration was changed. We will re-assign a
			// tracker to the peripheral. If the UID does not match, the peripheral was exchanged and we need to enter
			// the appropriate error state.
			// After setting the warning below, this will happen in the ::handleCoreWarnings routine, which is part of the
			// main logic loop and is called when there is time to process.
			//core_setWarning(CORE_WARNING_TRACKER_LOST, nOfCheckedPeripherals-logic->nOfSupportedPeripherals);
		} else {
			// undefined error
			core_setStatus(CORE_ERROR_PERIPHERAL_TRACKING);
		}
	}
}
