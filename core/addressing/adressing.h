/**
 * @file adressing.h
 *
 * @date Sep 27, 2021
 * @author A.Niggebaum
 *
 * @brief Addressing related functionality as well as presence tracking of peripherals.
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_ADRESSING_ADRESSING_H_
#define CORE_ADRESSING_ADRESSING_H_

/**
 * Represents a peripheral in the lookup table.
 */
typedef struct corePeripheralEntry {
  uint8_t address;                                                              //!< address of peripheral. 0x00 is invalid and marks empty entry
  uint8_t tracker;                                                        		//!< core assigned byte to tracker power cuts in peripheral
  uint32_t UID;                                                                 //!< individual identifier checksum for peripheral
} PeripheralAddressEntry_t;

/**
 * Restores the address table from memory (if any) and returns the number of found entries.
 *
 * @return Number of found entries
 */
int8_t adressing_recoverAddressTable(void);

/**
 * Store the currently RAM held address table to the persistent memory and overwrite the previously stored table there
 *
 * @param numberOfPeripherals Number of peripheral entries to store.
 * @return ::ILB_HAL_OK on success, error code otherwise.
 */
ILB_HAL_StatusTypeDef addressing_storeAddressTableInMemory(uint8_t numberOfPeripherals);

/**
 * Get an entry from the address table for a specific entry.
 *
 * @param index Index of entry to return, starting at 0.
 * @return Entry at index in table, NULL if the index exceeds the current size of the table.
 */
PeripheralAddressEntry_t* addressing_getAddressTableEntry(uint8_t index);

/**
 * Get the index of the peripheral table for the matching address.
 *
 * @param address Address to search for
 * @return The index of the matching index, 0xFF if no entry was found
 */
uint8_t addressing_getIndexOfAddress(uint8_t address);

/**
 * Return the address of a peripheral at the given address table index.
 *
 * @param index Index in address table from which to return the address
 * @return Address at given address table entry or 0xFF if index is outside of table boundes.
 */
uint8_t addressing_getAddressForIndex(uint8_t index);

/**
 * Get the assigned tracker value for an address table entry
 *
 * @param addressIndex Index in address table to which the tracker should be returned.
 * @return Current tracker of peripheral or 0x00 if index is outside of bounds of address table
 */
uint8_t addressing_assingTrackerValueToAddress(uint8_t addressIndex);

/**
 * Get the address for a peripheral at a specific address table index
 *
 * @param index Index of table entry from which to return the address
 * @return Address of stored entry or 0xFF if index is out of address table bounds.
 */
uint8_t addressing_getAddressForPeripheral(uint8_t index);

#endif /* CORE_ADRESSING_ADRESSING_H_ */
