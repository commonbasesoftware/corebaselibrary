/*
 * adressing.c
 *
 *  Created on: Sep 27, 2021
 *      Author: A.Niggebaum
 */

#include <stdint.h>
#include <stddef.h>
#include "ilb.h"
#include "adressing.h"
#include "../../settings/settings.h"
#include "../../logic_interface.h"
#include "../interface/interface.h"
#include "../memory/memory.h"

//
//	Private variables
//
extern const NetworkLayoutTypeDef logic_networkLayout;
//static PeripheralAddressEntry_t* coreAddressLookupTable;
static PeripheralAddressEntry_t coreAddressLookupTable[SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS];

/**
 * Recover a previously stored address table and return the number of loaded entries
 *
 * @return Number of peripheral loaded from memory or -1 in case of logic or loading error
 */
int8_t adressing_recoverAddressTable(void)
{
	// Ensure that the number of peripherals in the logic does not exceed the maximal number of supported peripherals.
	// Exceeding this number will cause segmentation faults. Dynamic allocation of the structures via malloc yields to
	// compilation errors, because at the final step, two libraries are joined with a linker file and the linker fails
	// linking the standard libraries.
	if (logic_networkLayout.totalNumberOfPeripherals > SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS)
	{
		return -1;
	}

	// prepare the table
	for (uint8_t i=0; i<logic_networkLayout.totalNumberOfPeripherals; i++)
	{
		coreAddressLookupTable[i].address = 0;
	}

	// read from memory
	uint32_t tmp = 0;
	ILB_HAL_StatusTypeDef success = bsw_EEPROMInterface(EEPROM_VAR_BYTE, EEPROM_NUMBER_OF_PERIPHERALS_BYTE_POS, &tmp);

	uint8_t nOfStoredPeripherals = (uint8_t)tmp;

	for (uint8_t i=0; i<(uint8_t)tmp; i++)
	{
		tmp = 0;
		success = bsw_EEPROMInterface(EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		coreAddressLookupTable[i].address = (uint8_t)(tmp);
		uint32_t UID = 0;
		success = bsw_EEPROMInterface(EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 1 + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		UID |= (tmp << 24);
		success = bsw_EEPROMInterface(EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 2 + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		UID |= (tmp << 16);
		success = bsw_EEPROMInterface(EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 3 + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		UID |= (tmp << 8);
		success = bsw_EEPROMInterface(EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 4 + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		UID |= tmp;
		coreAddressLookupTable[i].UID = UID;
	}

	if (success == ILB_HAL_OK)
	{
		return nOfStoredPeripherals;
	}

	return -1;
}

/**
 * Store address in memory
 */
ILB_HAL_StatusTypeDef addressing_storeAddressTableInMemory(uint8_t numberOfPeripherals)
{
	// write the complete address table to eeprom memory

	// store number of entries
	uint32_t tmp = numberOfPeripherals;
	ILB_HAL_StatusTypeDef status = bsw_EEPROMInterface(EEPROM_VAR_WRITE | EEPROM_VAR_BYTE, EEPROM_NUMBER_OF_PERIPHERALS_BYTE_POS, &tmp);

	// iterate through entries
	for (uint8_t i=0; i<numberOfPeripherals; i++)
	{
		// note that when we use bytes, we don't need to align to words
		tmp = (uint32_t)coreAddressLookupTable[i].address;
		status |= bsw_EEPROMInterface(EEPROM_VAR_WRITE | EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		tmp = ((coreAddressLookupTable[i].UID >> 24) & 0xFF);
		status |= bsw_EEPROMInterface(EEPROM_VAR_WRITE | EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 1+ i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		tmp = ((coreAddressLookupTable[i].UID >> 16) & 0xFF);
		status |= bsw_EEPROMInterface(EEPROM_VAR_WRITE | EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 2 + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		tmp = ((coreAddressLookupTable[i].UID >> 8) & 0xFF);
		status |= bsw_EEPROMInterface(EEPROM_VAR_WRITE | EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 3 + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
		tmp = (coreAddressLookupTable[i].UID & 0xFF);
		status |= bsw_EEPROMInterface(EEPROM_VAR_WRITE | EEPROM_VAR_BYTE, EEPROM_ADDRESS_TABLE_START_POS + 4 + i*CORE_BYTES_PER_LOOKUP_TABLE_ELEMENTS, &tmp);
	}

	return status;
}


/**
 * Accessor function for address table
 *
 * @param index
 * @return
 */
PeripheralAddressEntry_t* addressing_getAddressTableEntry(uint8_t index)
{
	if (index >= logic_networkLayout.totalNumberOfPeripherals)
	{
		return NULL;
	}
	return &coreAddressLookupTable[index];
}

/**
 * Accessor for index of address
 * @param address
 * @return
 */
uint8_t addressing_getIndexOfAddress(uint8_t address)
{
	for(uint8_t i=0; i<logic_networkLayout.totalNumberOfPeripherals; i++)
	{
		if (coreAddressLookupTable->address == address)
		{
			return i;
		}
	}
	return 0xFF;
}

/**
 * Accessor to address via index
 * @param index
 * @return
 */
uint8_t addressing_getAddressForIndex(uint8_t index)
{
	if (index < logic_networkLayout.totalNumberOfPeripherals)
	{
		return coreAddressLookupTable[index].address;
	}
	return 0xFF;
}

/**
 * Get a new tracker sequence for a peripheral at an index in the ::coreAddressLookupTable
 * This function also STORES the value at the appropriate location in the table
 *
 * @param index in ::coreAddressLookupTable
 *
 * @retval 8bit tracker variable
 */
uint8_t addressing_assingTrackerValueToAddress(uint8_t addressIndex)
{
	if (addressIndex > logic_networkLayout.totalNumberOfPeripherals) {
		return 0;
	} else {
		uint32_t now = bsw_getTick();
		coreAddressLookupTable[addressIndex].tracker = (((coreAddressLookupTable[addressIndex].UID >> 8) & 0xFF) ^ (coreAddressLookupTable[addressIndex].UID & 0xFF) ^ (now & 0xFF));
		return coreAddressLookupTable[addressIndex].tracker;
	}
}

/**
 * Get the address of the peripheral at the given index in the lookup table
 *
 * @param index of lookup table
 *
 * @retval address of peripehral. If out of range, return 0xFF (0x00 not good, because it is equivalent to a broadcast address)
 */
uint8_t addressing_getAddressForPeripheral(uint8_t index)
{
	if (index > logic_networkLayout.totalNumberOfPeripherals) {
		return 0xFF;
	}
	return coreAddressLookupTable[index].address;
}

uint8_t coreLib_getAddressForIndex(uint8_t index)
{
	return addressing_getAddressForIndex(index);
}
