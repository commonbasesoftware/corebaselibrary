/*
 * logic.c
 *
 *  Created on: Sep 27, 2021
 *      Author: A.Niggebaum
 *
 * Contains the main logic routine for a core
 *
 */

#include "ilb.h"
#include "ilbstd.h"
#include "core.h"
#include "core_blinkPatterns.h"
#include "settings/settings.h"
#include "addressing/adressing.h"
#include "discovery/discovery.h"
#include "network/network.h"
#include "register/register.h"
#include "broadcast/broadcast.h"
#include "../settings/settings.h"
#include "persistentVariables/persistentVariables.h"

#include "../logic_interface.h"

//
//	Private variables
//
static CoreStatus_t coreStatus = CORE_UNINITIALISED;
static CoreStatus_t lastCoreStatus = CORE_UNINITIALISED;

//
//	Variables for initialisation
//	These variables point to memory locations and are defined in the linker file
//
#ifndef UNIT_TEST
extern uint32_t _sidata;
extern uint32_t _sdata;
extern uint32_t _edata;
extern uint32_t _sbss;
extern uint32_t _ebss;
extern uint32_t _interface_location;
static BaseSoftwareInterface_t* interface_base;		// this variable will be filled with the structure encountered at _interface_location
#else
// in case we are compiling for the unit test, the interface structure is defined in the unit test program
// and the program also takes care of proper initialisation of the values.
extern BaseSoftwareInterface_t* interface_base;
#endif

// extern variables for logic
extern const NetworkLayoutTypeDef logic_networkLayout;

//
//	Private variables
//
static int8_t nOfStoredPeripherals = 0;

//
//	Actually create the interface functions
//
sendMessage_t			bsw_messaging_sendMessage;
sendResponse_t			bsw_messaging_respondCode;
scheduleTask_t			bsw_timer_scheduleTask;
scheduleSlowTask_t		bsw_timer_scheduleSlowTask;
getTick_t				bsw_getTick;
delay_t					bsw_delay;
getRandomNumber_t		bsw_getRandomNumber;
getLightEngineStatus_t	bsw_lightEngine_getStatus;
startLightEngineFade_t	bsw_lightEngine_startFade;
eepromInterface_t		bsw_EEPROMInterface;
telemetryInterface_t	bsw_telemetryInterface;

//
//	Private function prototypes
//

/**
 * Primary initialisation routine
 *
 * This piece of software is not part of the original compilatio image. Therefore no variables are initialised or zeroed when
 * the section of the software is entered. The initialisation of variables and the zeroing of variables is done here manually.
 *
 * @return
 */
ILB_HAL_StatusTypeDef core_init(uint8_t* nOfSlowTimers)
{
#ifndef UNIT_TEST
	//
	//	Variables declared in the logic part of the program are not initialised by the default Reset_Handler() routine.
	//  In order for all variables to be initialised correctly, we need to to this manually. We can use the
	//  locations defined in the linker script.
	//
	// read necessary values from flash to ram
	uint32_t *dataInit = &_sidata;
	uint32_t *dataTrgt = &_sdata;
	while (dataTrgt < &_edata)
	{
		*dataTrgt++ = *dataInit++;
	}

	uint32_t *bss = &_sbss;
	while (bss < &_ebss)
	{
		*bss++ = 0;
	}

	// build up the interface to the base software
	// we rely on the check from the base software that the interfaces match
	uint32_t* locationOfInterface = &_interface_location;
	interface_base = (BaseSoftwareInterface_t*)(locationOfInterface);
#else
	// Compiling for unit test use. The unit test code will map the functions to simulated or other implementations
#endif

	// initialise interface functions
	bsw_messaging_sendMessage 		= interface_base->sendMessage;
	bsw_messaging_respondCode 		= interface_base->sendResponseCode;
	bsw_timer_scheduleTask	 		= interface_base->scheduleSystemTask;
	bsw_timer_scheduleSlowTask 		= interface_base->scheduleSlowTask;
	bsw_getTick    	 				= interface_base->getTick;
	bsw_delay			 			= interface_base->delay;
	bsw_getRandomNumber 			= interface_base->getRandomNumber;
	bsw_lightEngine_getStatus		= interface_base->getLightEngineStatus;
	bsw_lightEngine_startFade		= interface_base->startLightEngineFade;
	bsw_EEPROMInterface 			= interface_base->eepromInterface;
	bsw_telemetryInterface 			= interface_base->telemetryInterface;

	// communicate number of slow timers
	*nOfSlowTimers = logic_networkLayout.numberOfSlowTimers + CORE_BASE_TIME_NUMBER_OF;

	return ILB_HAL_OK;
}

/**
 * Primary routine for core
 * @return
 */
uint8_t core_logic(void)
{
	// handle change of state
	CoreStatus_t nextCoreStatus = coreStatus;

	switch (coreStatus)
	{
	case CORE_UNINITIALISED:
	{
		// before we do anything, allow peripherals to boot
		bsw_delay(SETTING_PERIPHERAL_BOOT_ALLOWANCE_MS);


		// check that we are dealing with a compatible logic interface
		// this check should not be necessary since we are checking from "the other side" as well
		// and this routine should only be called if the versions match. However, just to make sure...

		// start broadcast messages. We also need them to inicate logic or other errors
		broadcast_startBroadcastMessages();

		// TODO: check logic interface

		// TODO: special case, no peripherals. Jump straight to initialised case

		// check if we have configured a network previously
		nOfStoredPeripherals = adressing_recoverAddressTable();
		// check for logic errors
		if (nOfStoredPeripherals < 0)
		{
			nextCoreStatus = CORE_LOGIC_ERROR;
			break;
		}

		// number of entries in table matches. We must assume that this is ok
		if (nOfStoredPeripherals != 0)
		{
			// double check number match
			if (nOfStoredPeripherals != logic_networkLayout.totalNumberOfPeripherals)
			{
				nextCoreStatus = CORE_ERROR_PERIPHERAL_MISSING;
				break;
			}
			// we need to check the addresses that have been loaded
			nextCoreStatus = CORE_CHECK_PERIPHERALS;
			break;
		}

		// if we reached here, no address table entries have been found in the memory. Start network discover
		nextCoreStatus = CORE_WAITING_FOR_PERIPHERAL_UID;
		discovery_startNetworkDiscovery();
		break;
	}

	// we are waiting for the peripherals to report their UID as part of the network discovery process.
	// the starting routine manages the state of the core
	case CORE_WAITING_FOR_PERIPHERAL_UID:
		switch(discovery_getState())
		{
		case ILB_HAL_ERROR:
			nextCoreStatus = CORE_INITIALISATION_ERROR;
			break;

		case ILB_HAL_INVALID:
			nextCoreStatus = CORE_INITIALISATION_ERROR_NUMBER_MISMATCH;
			break;

		case ILB_HAL_OK:
			// process is done. Start the configuration
			discovery_startPeripheralCofiguration();
			nextCoreStatus = CORE_CONFIGURING_PERIPHERALS;
			break;

		default:
			break;
		}
		break;

	// we are in the peripheral configuration process
	case CORE_CONFIGURING_PERIPHERALS:
		// check the status of the configuration process
		// the re-entrant function discovery_configurePeripherals() will manage the core state in case of an error
		switch (discovery_configurePeripherals())
		{
		case ILB_HAL_OK:
			// store address table
			if (addressing_storeAddressTableInMemory(logic_networkLayout.totalNumberOfPeripherals) == ILB_HAL_OK)
			{
				nextCoreStatus = CORE_INITIALISED;
				break;
			}
			// the re-entrant function also manages the state of the core
		case ILB_HAL_BUSY:
			// nothing to do. We are waiting for an answer
			break;

		case ILB_HAL_ERROR:
			// initialisation error
			nextCoreStatus = CORE_INITIALISATION_ERROR;
			break;

		default:
			// error state
			nextCoreStatus = CORE_INITIALISATION_ERROR;
		}
		break;

	// we are waiting for the configuration of the peripherals to finish
	case CORE_WAITING_FOR_PERIPHERAL_CONFIG:
		// nothing to do
		break;

	// check the peripherals
	case CORE_CHECK_PERIPHERALS:
	{
		ILB_HAL_StatusTypeDef networkCheckStatus = networkCheck_confirmUIDOfPeripherals();
		// check if process is ongoing
		if (networkCheckStatus == ILB_HAL_BUSY)
		{
			// nothing to do. process is still ongoing. We are probably waiting for an answer
			break;
		}
		// check if an error occured
		else if (networkCheckStatus == ILB_HAL_ERROR)
		{
			nextCoreStatus = CORE_INITIALISATION_ERROR;
			break;
		}
		// check for wrong peripherals
		else if (networkCheckStatus == ILB_HAL_INVALID)
		{
			nextCoreStatus = CORE_ERROR_PERIPHERAL_MISSING;
			core_setStatus(CORE_ERROR_PERIPHERAL_MISSING);
		}
		// check for normal exit
		else if (networkCheckStatus == ILB_HAL_OK)
		{
			// process has terminated as intended
			nextCoreStatus = CORE_INITIALISED;
			break;
		}
	}
		break;

	// core running normally
	case CORE_INITIALISED:
		// check if we entered this case. If so, we need to do some initialisation tasks
		if (lastCoreStatus != CORE_INITIALISED)
		{
			// start the periodic network check
			networkCheck_startPeriodicNetworkCheck();

			// emit start operation
			messaging_sendStartOperation();

			// start managing the light engines
			if (lightEngine_initManager() != ILB_HAL_OK)
			{
				nextCoreStatus = CORE_INITIALISATION_ERROR;
				break;
			}

			// indicate that we are done
			interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_GREEN_ONESHOT, 0xFF, 0x00);

			// prepare persistent variables interface
			persistentVariables_init();

			// start the logic
			logic_start();

			// send a broadcast message
			broadcast_queueBroadcastMessage();

			// start pushing status information
			informationDistribution_start();
		}
		// execute the main event loop
		logic_eventLoop();

		break;

	case CORE_INITIALISATION_ERROR:
		// nothing to do here.
		break;

	default:
		break;
	}

	// track changes
	lastCoreStatus = coreStatus;

	// check if a change is required
	if (coreStatus != nextCoreStatus)
	{
		core_setStatus(nextCoreStatus);
	}

	// handle broadcast messages
	broadcast_handle();

	return coreStatus;
}

/**
 * Setter function for core status.
 *
 * Handles eventual indication patterns and other tasks.
 *
 * @param status
 */
void core_setStatus(CoreStatus_t status)
{
	// prevent change back from factory reset state
	if (coreStatus == CORE_EXECUTE_FACTORY_RESET) {
		return;
	}

	/// The core is set to the following states
	switch (status) {
	/// - ::CORE_WAITING_FOR_PERIPHERAL_UID: The core is waiting for the reported UIDs of peripherals in the network
	case CORE_WAITING_FOR_PERIPHERAL_UID:
		break;
		/// - ::CORE_CONFIGURING_PERIPHERALS: The core is in the process of configuring the peripherals
	case CORE_CONFIGURING_PERIPHERALS:
		break;
		/// - ::CORE_WAITING_FOR_PERIPHERAL_CONFIG: The core is waiting for the peripheral to acknowledge the configuration
	case CORE_WAITING_FOR_PERIPHERAL_CONFIG:
		break;
		/// - ::CORE_CHECK_PERIPHERALS: The core is checking the presence of the peripherals in the network
	case CORE_CHECK_PERIPHERALS:
		break;
		/// - ::CORE_INITIALISED: The core is fully initialised. This is the normal operation state
	case CORE_INITIALISED:
		break;
		/// - ::CORE_INITIALISATION_ERROR: An unspecified error occurred during the initialisation. The system needs to be reset or power cycled
	case CORE_INITIALISATION_ERROR:
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_INITIALISATION_ERROR_GREEN, BLINK_PATTERN_INITIALISATION_ERROR_RED);
		lightEngine_setSaveState(BLINK_ATTACHED_LIGHT_ENGINE);
		broadcast_queueBroadcastMessage();
		break;
		/// - ::CORE_INITIALISATION_ERROR_NUMBER_MISMATCH: During initialisation it was found that the ILB network does not contain the expected number of peripherals
	case CORE_INITIALISATION_ERROR_NUMBER_MISMATCH:
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_NUMBER_MISMATCH_GREEN, BLINK_PATTERN_ERROR_NUMBER_MISMATCH_RED);
		lightEngine_setSaveState(BLINK_ATTACHED_LIGHT_ENGINE);
		broadcast_queueBroadcastMessage();
		break;
		/// - ::CORE_INITIALISATION_ERROR_WRONG_PERIPHERALS: During initialisation it was found that the wrong types of peripherals were found
	case CORE_INITIALISATION_ERROR_WRONG_PERIPHERALS:
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_PERIPHERAL_TYPE_MISMATCH_GREEN, BLINK_PATTERN_ERROR_PERIPHERAL_TYPE_MISMATCH_RED);
		lightEngine_setSaveState(BLINK_ATTACHED_LIGHT_ENGINE);
		broadcast_queueBroadcastMessage();
		break;
		/// - ::CORE_LOGIC_ERROR: An unspecified error occurred in the logic that caused the core to leave the normal logic loop
	case CORE_LOGIC_ERROR:
		break;
		/// - ::CORE_ERROR_PERIPHERAL_TRACKING: An error occurred while regularly checking if the same peripherals are still in the network.
	case CORE_ERROR_PERIPHERAL_TRACKING:
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_PERIPHERAL_TRACKING_GREEN, BLINK_PATTERN_ERROR_PERIPHERAL_TRACKING_RED);
		lightEngine_setSaveState(BLINK_ATTACHED_LIGHT_ENGINE);
		// notify network
		broadcast_queueBroadcastMessage();
		break;
		/// - ::CORE_ERROR_PERIPHERAL_DOUBLE_ADDRESS: An error occurred during the network re-establisation where multiple peripherals seem to answer to the same address
	case CORE_ERROR_PERIPHERAL_DOUBLE_ADDRESS:
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_PERIPHERAL_TRACKING_GREEN, BLINK_PATTERN_ERROR_PERIPHERAL_TRACKING_RED);
		lightEngine_setSaveState(BLINK_ATTACHED_LIGHT_ENGINE);
		// notify network
		broadcast_queueBroadcastMessage();
		break;
		/// - ::CORE_ERROR_PERIPHERAL_MISSING: An error occurred during initial or periodic network check. An expected peripheral was not found
	case CORE_ERROR_PERIPHERAL_MISSING:
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_PERIPHERAL_MISSING_GREEN, BLINK_PATTERN_ERROR_PERIPHERAL_MISSING_RED);
		lightEngine_setSaveState(BLINK_ATTACHED_LIGHT_ENGINE);
		// notify network
		broadcast_queueBroadcastMessage();
		break;
		/// - ::CORE_ERROR_PERIPHERAL_CHECK: An error occurred while checking for the peripherals in the network.
	case CORE_ERROR_PERIPHERAL_CHECK:
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_PERIPHERAL_CHECK_GREEN, BLINK_PATTERN_ERROR_PERIPHERAL_CHECK_RED);
		lightEngine_setSaveState(BLINK_ATTACHED_LIGHT_ENGINE);
		// notify network
		broadcast_queueBroadcastMessage();
		break;
		/// - ::CORE_EXECUTE_FACTORY_RESET: The core is executing a factory reset
	case CORE_EXECUTE_FACTORY_RESET:
		// set the indicators
		// TODO: special tasks here
		break;
		/// - ::CORE_EXECUTE_SETTINGS_RESET: The core is executing a settings reset
	case CORE_EXECUTE_SETTINGS_RESET:
		// set the indicators
		// TODO: special tasks here
		break;
		/// - ::CORE_EXECUTE_RESET: The core is executing a power reset
	case CORE_EXECUTE_RESET:
		// set the indicators
		// TODO: special tasks there
		break;
		/// - ::CORE_ERROR_OPEN_CIRCUIT: The core has detected an open circuit on the attached light engine
	case CORE_ERROR_OPEN_CIRCUIT:
		// open circuit detected
		// set the status byte
		register_core_setSystemFlag(ILB_SYSTEM_REPORT_LAMP_ERROR_FLAG);
		// publish
		broadcast_queueBroadcastMessage();
		// set indicator
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_OPEN_CIRCUIT_GREEN, BLINK_PATTERN_ERROR_OPEN_CIRCUIT_RED);
		break;

		/// - ::CORE_ERROR_SHORT_CIRCUIT: The core has detected a short circuit on the attached light engine
	case CORE_ERROR_SHORT_CIRCUIT:
		// short circuit detected
		// set the status bit
		register_core_setSystemFlag(ILB_SYSTEM_REPORT_LAMP_FAILURE_FLAG);
		// publish
		broadcast_queueBroadcastMessage();
		// set indicator
		interface_base->setIndicator(DEVICE_GREEN_INDICATOR | DEVICE_RED_INDICATOR, BLINK_PATTERN_ERROR_SHORT_CIRCUIT_GREEN, BLINK_PATTERN_ERROR_SHORT_CIRCUIT_RED);
		break;

	default:
		break;
	}

	// set the status
	coreStatus = status;

	// set the register
	register_core_setSystemStatus(status);
}

/**
 * Accessor to status of core
 * @return
 */
CoreStatus_t core_getStatus(void)
{
	return coreStatus;
}
