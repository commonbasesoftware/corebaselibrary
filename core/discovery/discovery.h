/**
 * @file discovery.h
 *
 * @date Sep 27, 2021
 * @author A.Niggebaum
 *
 * @brief Network discovery and formation routines.
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_DISCOVERY_DISCOVERY_H_
#define CORE_DISCOVERY_DISCOVERY_H_

#include "ilb.h"

/**
 * Starts the overall network discovery process. The routine steps through the following steps
 * 1) Discover devices in network
 * 2) Assign addresses to devices in network
 * 3) Assign tracker to devices in network
 * 4) Send configuration messages to devices in network
 * 5) Send ::ILB_START_OPERATION broadcast message.
 * A failure to respond or write settings will cause an initialisation error. The status of the overall process should be checked by retularly calling the discovery_getState() routine.
 * This function is NON-BLOCKING.
 */
void discovery_startNetworkDiscovery(void);

/**
 * Used to probe the status of the configuration mechanism. The interpretation of the return values is:
 * - ::ILB_HAL_OK: Process has terminated and was successful. The state machine can progress
 * - ::ILB_HAL_BUSY: Process is ongoing. The state machine must probe again before continuing
 * - any error code: The process failed. The core was not able to form a network. An error state should be entered
 * @return Current status
 */
ILB_HAL_StatusTypeDef discovery_getState(void);

/**
 * Starts the peripheral configuration routine. The network information contains the messages that must be written to peripherals for configuration. The routine will iterate through the messages in the following order
 * 1) Select first peripheral. If no peripheral needs configuration, terminate.
 * 2) Select first message of selected peripheral
 * 3) Send selected message as write message
 * 4) Wait for ACK response. If no ACK response is received, abort with error. If ::ILB_OK is received, proceed.
 * 5) Select next message. If no further messages are in the list, select the next peripheral and go to 2). If no further peripherals are in the list, exit with success.
 * This function is NON-BLOCKING and discovery_configurePeripherals() must be called periodically to get the status. The state machine must not advance until the process is complete.
 */
void discovery_startPeripheralCofiguration(void);

/**
 * Get the status of the configuration process. The interpretation of the return values is:
 * - ::ILB_HAL_OK: Process has terminated and was successful. The state machine can progress
 * - ::ILB_HAL_BUSY: Process is ongoing. The state machine must probe again before continuing
 * - any error code: The process failed. The core was not able to form a network. An error state should be entered
 * @return Current status
 */
ILB_HAL_StatusTypeDef discovery_configurePeripherals(void);

#endif /* CORE_DISCOVERY_DISCOVERY_H_ */
