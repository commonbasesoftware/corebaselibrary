/*
 * discovery.c
 *
 *  Created on: Sep 27, 2021
 *      Author: A.Niggebaum
 */

#include <stddef.h>

#include "ilb.h"
#include "../core.h"
#include "../addressing/adressing.h"
#include "../messaging/messaging.h"
#include "../interface/interface.h"
#include "../../logic_interface.h"
#include "../../settings/settings.h"
#include "../../logic_interface.h"

//
//	Interface to logic. The following constants must be defined in the logic library
//
extern const NetworkLayoutTypeDef logic_networkLayout;
extern const logicConfigurationTypeDef logic_configurationInstructionSet;

typedef enum ConfigurationState {
	CONFIGURATION_RUNNING,
	CONFIGURATION_WAITING,
	CONFIGURATION_DONE,
	CONFIGURATION_ERROR,
	CONFIGURATION_INVALID,		// special invalid error
} ConfigurationState_t;

//
//	Private variables
//
static ConfigurationState_t configState = CONFIGURATION_RUNNING;

static uint8_t nOfKnownPeripherals = 0;
static uint16_t nOfConfiguredPeripherals = 0;

static uint8_t currentConfigurationIndex = 0;				//!< current index of configuration messagse. This does include tracker update messages
static uint8_t successfullConfigurationMessages = 0;		//!< tracker of acknowledged configuration messages. This does include tracker update messages

//static uint8_t* nOfFoundPeripheralsInGroups;				//!< array that will hold the number of peripherals found in each group. The number elements corresponds to the number of groups, the indices will match
static uint8_t nOfFoundPeripheralsInGroups[SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS] = {0};

//
//	Private function prototypes
//
static void discoveryCallback(ILBMessageStruct* msg);
static void discovery_comissioningWindowHasExpired(void);

static void assignAddressCallback(ILBMessageStruct* message);
static void assignAddressTimeoutCallback(void);

static void discovery_handleConfigurationFeedback(ILBMessageStruct* message);
static void discovery_configurationWindowHasExpired(void);

static void generateRandomAddress(uint8_t *targetVariable);

/**
 * Start the network discvovery procedure
 */
void discovery_startNetworkDiscovery(void)
{
	// prepare working variables
	nOfKnownPeripherals = 0;
//	nOfFoundPeripheralsInGroups = malloc(sizeof(*nOfFoundPeripheralsInGroups) * logic_networkLayout.numberOfGroups);
	for (uint8_t i=0; i<logic_networkLayout.numberOfGroups; i++)
	{
		nOfFoundPeripheralsInGroups[i] = 0;
	}

	// send the discovery message into the network
	ILBMessageStruct msg;
	msg.address = ILB_MESSAGE_BROADCAST_ADDRESS;
	msg.identifier = ILB_GENERATE_IDENTIFIER;
	msg.messageType = ILB_MESSAGE_COMMAND;
	msg.payloadSize = 1;
	msg.payload[0] = ILB_REPORT_UID;

	// send discovery message
	bsw_messaging_sendMessage(&msg, discoveryCallback, SETTING_NETWORK_DISCOVERY_TIMEOUT_MS, discovery_comissioningWindowHasExpired);

	// set the state
	configState = CONFIGURATION_WAITING;
}

/**
 * Return the state of the discovery process
 *
 * @return
 */
ILB_HAL_StatusTypeDef discovery_getState(void)
{
	if (configState == CONFIGURATION_DONE)
	{
		return ILB_HAL_OK;
	} else if (configState == CONFIGURATION_ERROR)
	{
		return ILB_HAL_ERROR;
	} else if (configState == CONFIGURATION_INVALID)
	{
		return ILB_HAL_INVALID;
	}
	return ILB_HAL_BUSY;
}

/**
 * Callback function to discovery message send into network
 *
 * We store the replied UID in the (currently empty) address list. We also start filling the address table in the right
 * order, which is imposed by the address indices demanded by the logic! The logic structure contains the information which
 * address should go to which index. It is imperative that we obey these numbers.
 *
 * @param msg
 */
void discoveryCallback(ILBMessageStruct* message)
{
	// check if the answer is the answer to a query UID message
	if (message->identifier == ILB_MESSAGE_IDENTIFIER_MIN &&
		message->payloadSize == 4) {

		// count up number of known peripherals
		nOfKnownPeripherals++;

		// the address of the message contains the identifier. Check if this identifier exists in the lists of expected group IDs
		for (uint8_t i=0; i<logic_networkLayout.numberOfGroups; i++)
		{
			// check for match
			if (logic_networkLayout.groups[i]->groupID == message->address)
			{
				// match found. Store at the UID at the  appropriate index

				//layout->groups[i]->nOfFound++;
				nOfFoundPeripheralsInGroups[i] += 1;

				// check if we exceed the number
				if (nOfFoundPeripheralsInGroups[i] > logic_networkLayout.groups[i]->nOfExpected)
				{
					configState = CONFIGURATION_INVALID;
					return;
				}

				// the peripheral is a candidate. Extract the UID
				uint32_t tmpUID = 0;
				for (uint8_t i=0; i<4; i++) {
					tmpUID += (message->payload[i] << ((3-i)*8));
				}

				// store the UID in the address table for later address assignment.
				// the index matches the specified index for the logic
				PeripheralAddressEntry_t* entry = addressing_getAddressTableEntry(logic_networkLayout.groups[i]->addressIndexList[nOfFoundPeripheralsInGroups[i]-1]);
				if (entry == NULL)
				{
					configState = CONFIGURATION_ERROR;
					return;
				}
				entry->UID = tmpUID;
				entry->address = message->address;		// this is the peripheral class for the moment.
			}
		}
		// register this function again so that other peripherals can register
		message->messageType = ILB_MESSAGE_HANDLER_ONLY;
		bsw_messaging_sendMessage(message, discoveryCallback, SETTING_NETWORK_DISCOVERY_TIMEOUT_MS, discovery_comissioningWindowHasExpired);
	}
}

/**
 * Callback function when commissioning window has expired
 *
 * The function will check the detected peripherals against the definition of the network. The criterions are:
 * - Correct number of peripherals
 * - Correct number and type of peripherals
 * If everything matches, the peripherals are given addresses. The messages are send out in bulk here, the feedback
 * is handled in assignAddressCallback() and assignAddressTimeoutCallback() with a timeout of ::CORE_ADDRESS_NEGOTIATION_TIME_MS.
 * Said functions will change the initialization status of the core and instruct it to proceed with the configuration of the network.
 *
 */
void discovery_comissioningWindowHasExpired(void)
{
	CoreStatus_t status = core_getStatus();
	switch(status) {
	case CORE_WAITING_FOR_PERIPHERAL_UID:
		// the core was waiting for the peripherals. Check if the found network satisfies the requirements
		// check for total number of peripherals
		if (nOfKnownPeripherals != logic_networkLayout.totalNumberOfPeripherals) {
			// set error status
			configState = CONFIGURATION_INVALID;
			return;
		}
		// check for matching individual numbers
		for (uint8_t i=0; i<logic_networkLayout.numberOfGroups; i++) {
			if (nOfFoundPeripheralsInGroups[i] != logic_networkLayout.groups[i]->nOfExpected) {
				configState = CONFIGURATION_INVALID;
				return;
			}
		}
		// generate list of addresses for peripherals
		if (SETTING_PERIPHERAL_ADDRESS_MAX-SETTING_PERIPHERAL_ADDRESS_MIN < nOfKnownPeripherals) {
			// this is an error state. Not enough addresses available
			configState = CONFIGURATION_ERROR;
			return;
		}

		// create a list of random addresses
		uint8_t addressList[SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS] = {0};
		uint8_t nOfFoundAddresses = 0;
		while(nOfFoundAddresses < logic_networkLayout.totalNumberOfPeripherals) {
			uint8_t alreadyInList = 0;
			generateRandomAddress(&addressList[nOfFoundAddresses]);
			// check if address is already in the list
			for (uint8_t i=0; i<nOfFoundAddresses; i++) {
				if (addressList[nOfFoundAddresses] == addressList[i]) {
					// try again
					alreadyInList = 1;
				}
			}
			// do we need to try again
			if (alreadyInList) {
				continue;
			}
			// store in list
			nOfFoundAddresses++;
		}

		// if we reached here, the total number of peripherals and individual number of peripherals is valid.
		// iterate through the peripherals and assign them addresses. The order of the address lookup table
		// must be the same as defined in the enum. The order is stored in the network layout structure.

		for (uint8_t i=0; i<nOfKnownPeripherals; i++) {
			PeripheralAddressEntry_t* handledEntry;
			handledEntry = addressing_getAddressTableEntry(i);
			uint32_t UID = handledEntry->UID;

			// prepare and send message
			ILBMessageStruct msg;
			msg.address = ILB_MESSAGE_BROADCAST_ADDRESS;
			msg.identifier = ILB_GENERATE_IDENTIFIER;
			msg.messageType = ILB_MESSAGE_COMMAND;
			msg.payloadSize = 6;
			msg.payload[0] = ILB_CHANGE_ADDRESS;
			msg.payload[1] = (UID >> 24) & 0xFF;   // UID
			msg.payload[2] = (UID >> 16) & 0xFF;
			msg.payload[3] = (UID >> 8) & 0xFF;
			msg.payload[4] = (UID & 0xFF);
			msg.payload[5] = addressList[i];       // address to take

			bsw_messaging_sendMessage(&msg, &assignAddressCallback, SETTING_NETWORK_DISCOVERY_TIMEOUT_MS, assignAddressTimeoutCallback);

			// store in table
			handledEntry->address = addressList[i];
		}
		// change of initialisation state will be handled in callback when all peripherals have an assigned address

		// free the memory used previously
		//free(addressList);
		break;
	default:
		break;
	}
}

/**
 * Callback function to assign addresses
 */
void assignAddressCallback(ILBMessageStruct* message)
{
	// check if the device returned an ILB ok
	if (!(message->messageType == ILB_MESSAGE_ACHKNOWLEDGE &&
		  message->payload[0] == ILB_OK &&
		  message->payloadSize == 1)) {
		// indicate error while trying to assign the addresses
		configState = CONFIGURATION_ERROR;
	}
	// track number of peripherals that have acknowledged
	nOfConfiguredPeripherals++;
	if (nOfConfiguredPeripherals == logic_networkLayout.totalNumberOfPeripherals) {
		// we have assigned addresses to all peripherals. Proceed to configuration
		configState = CONFIGURATION_DONE;
	}
}

/**
 * Timeout function to assign addresses
 */
void assignAddressTimeoutCallback(void)
{
	// we ran into a timout. The addresses could not be assigned in time
	configState = CONFIGURATION_ERROR;
}

/**
 * Start the network configuration, the transmission of settings and writing of tracker bytes.
 *
 * This function only serves as the starter. The actual work is done by the re-entrant function discovery_configurePeripherals.
 */
void discovery_startPeripheralCofiguration(void)
{
	currentConfigurationIndex = 0;
	configState = CONFIGURATION_RUNNING;
}

/**
 * Commence the configuration of the peripherals
 *
 * This function will be re-entered by the ilbLogic function. It iterates throught he configuration commands and sends the requested
 * write commands to the peripherals. If the response is ILB_OK, the variable currentConfigurationIndex will be counted up. If the variable
 * is smaller than nOfTotalConfigurationMessages, the program knows that there are configuration messages waiting to be sent and proceeds
 * sending them. Once ::currentConfigurationIndex == ::nOfTotalConfigurationMessages, the program proceeds to query the configuration checksum
 * from the previously configured devices. Again, the variable currentConfigurationIndex is counted up. It will therefore reach
 * ::nOfTotalConfiguraitonMessagse + ::numberOfKnownPeripherals. If this state is reached, this is taken as the signal that all peripehrals
 * are configured, their checksum gathered and therefore the lookupTable is complete and can be stored in the eeprom.
 *
 * \todo This function should be moved to core.c. How should the variables be organised? Especially the link to the addresses?
 *
 * \retval 0 on success and full configuration. 1 if a re-entry is necessary
 */
ILB_HAL_StatusTypeDef discovery_configurePeripherals(void)
{

	if (configState == CONFIGURATION_DONE)
	{
		// signal all ok
		return ILB_HAL_OK;
	} else if (configState == CONFIGURATION_ERROR)
	{
		// signal that something went wrong
		return ILB_HAL_OK;
	} else if (configState == CONFIGURATION_WAITING)
	{
		return ILB_HAL_BUSY;
	}

	//
	//
	//
	// the core can send the next configuration message
	if (currentConfigurationIndex < logic_configurationInstructionSet.totalNumberOfConfigurationMessages) {
		// prepare message the next message.
		// we need to iterate through the structure to find the next
		uint8_t counter = 0;
		for (uint8_t setIndex = 0; setIndex < logic_configurationInstructionSet.numberOfConfigurationSets; setIndex++)
		{
			for (uint8_t instructionIndex = 0; instructionIndex < logic_configurationInstructionSet.instructionSets[setIndex].numberOfInstructsionInSet; instructionIndex++)
			{
				if (counter == currentConfigurationIndex)
				{
					// prepare message
					ILBMessageStruct configurationMessage;
					configurationMessage.address = addressing_getAddressForIndex(logic_configurationInstructionSet.instructionSets[setIndex].targetAddressIndex);
					configurationMessage.identifier = ILB_GENERATE_IDENTIFIER;
					configurationMessage.messageType = ILB_MESSAGE_WRITE;
					configurationMessage.payloadSize = 0;
					// we found the current message
					for (uint8_t i=0; i<logic_configurationInstructionSet.instructionSets[setIndex].instructionList[instructionIndex].numberOfBytesInInstruction; i++)
					{
						configurationMessage.payload[i] = logic_configurationInstructionSet.instructionSets[setIndex].instructionList[instructionIndex].instruction[i];
						configurationMessage.payloadSize++;
					}
					core_setStatus(CORE_WAITING_FOR_PERIPHERAL_CONFIG);

					// send message
					bsw_messaging_sendMessage(&configurationMessage, discovery_handleConfigurationFeedback, SETTING_NETWORK_APPLY_SETTING_TIMEOUT_MS, discovery_configurationWindowHasExpired);

					// proceed to next message
					currentConfigurationIndex++;
					// indicate that we are waiting for an anwer
					configState = CONFIGURATION_WAITING;
					break;
				}
				counter++;
			}
		}
	} else if (currentConfigurationIndex < logic_configurationInstructionSet.totalNumberOfConfigurationMessages + nOfConfiguredPeripherals) {
		// write the initial tracker to the peripheral
		core_setStatus(CORE_WAITING_FOR_PERIPHERAL_CONFIG);
		// prepare message
		ILBMessageStruct trackerWriteMessage;
		trackerWriteMessage.address = addressing_getAddressTableEntry(currentConfigurationIndex - logic_configurationInstructionSet.totalNumberOfConfigurationMessages)->address;
		trackerWriteMessage.messageType = ILB_MESSAGE_WRITE;
		trackerWriteMessage.identifier = ILB_GENERATE_IDENTIFIER;
		trackerWriteMessage.payload[0] = ILB_PERIPHERAL_STATUS_BASE_ADDRESS;
		trackerWriteMessage.payload[1] = ILB_TRACKER_SUB_ADDRESS;
		trackerWriteMessage.payload[2] = addressing_assingTrackerValueToAddress(currentConfigurationIndex - logic_configurationInstructionSet.totalNumberOfConfigurationMessages);
		trackerWriteMessage.payloadSize = 3;

		// send message
		bsw_messaging_sendMessage(&trackerWriteMessage, discovery_handleConfigurationFeedback, SETTING_NETWORK_APPLY_SETTING_TIMEOUT_MS, discovery_configurationWindowHasExpired);

		// count up the configuration index
		currentConfigurationIndex++;
		// indicate that we are waiting for an answer
		configState = CONFIGURATION_WAITING;
	} else if (currentConfigurationIndex < logic_configurationInstructionSet.totalNumberOfConfigurationMessages + 2*nOfKnownPeripherals){
		// signal success
		configState = CONFIGURATION_DONE;

	} else {
		// all done
		configState = CONFIGURATION_DONE;
	}
	return ILB_HAL_BUSY;
}

/**
 * A configuration message has been received and the peripheral has answered
 */
void discovery_handleConfigurationFeedback(ILBMessageStruct* message)
{
	// Check if we are writing trackers or processing configuration messages
	// if the tracker currentConfiguration runs up to the total number of configuration messages plus the number of peripherals
	// If it is larger than the number of configuration messages but smaller than this value plus the number of peripherals,
	// then the logic is writing tracker values to the peripherals

	// general check. See if the configuration messages are processed properly
	if (message->payloadSize == 1 && message->payload[0] != ILB_OK) {
		// error occured
		configState = CONFIGURATION_ERROR;
		return;
	}

	if (currentConfigurationIndex > logic_configurationInstructionSet.totalNumberOfConfigurationMessages) {
		// special handling
	}

	// we are processing

	configState = CONFIGURATION_RUNNING;		// indicate that the next message can be sent
	// count up the tracker
	successfullConfigurationMessages++;
}

/**
 * A configuration command has not been acknowledged by the peripehral i time
 */
void discovery_configurationWindowHasExpired(void)
{
	// report error to core logic
	configState = CONFIGURATION_ERROR;
}

/**
 * Get a random address that complies with the address range
 *
 * @param address of target variable
 */
void generateRandomAddress(uint8_t *targetVariable)
{
	// get a random number
	uint32_t randomNumber = 0;
	uint8_t newAddress = SETTING_PERIPHERAL_ADDRESS_MIN;			//!< fallback solution. Start low
	// loop in case the address already exists
	while (1) {
		if (bsw_getRandomNumber(&randomNumber) == ILB_HAL_OK) {
			// we generated a random number. Push it in the 8 bit format
			newAddress = (randomNumber & 0xFF);
		} else {
			// fallback. Random number generator is not working. Count up from base address
			newAddress++;
		}
		// check if the address complies with the boundaries
		if (newAddress < SETTING_PERIPHERAL_ADDRESS_MIN || newAddress > SETTING_PERIPHERAL_ADDRESS_MAX) {
			// try again
			continue;
		}
		// the address does not exist. Break
		break;
	}
	// assign value to address
	*targetVariable = newAddress;
}

