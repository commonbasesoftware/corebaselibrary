/*
 * messaging.c
 *
 *  Created on: Sep 27, 2021
 *      Author: A.Niggebaum
 */

#ifndef CORE_MESSAGING_C_
#define CORE_MESSAGING_C_

#include "ilb.h"
#include "messaging.h"
#include "../addressing/adressing.h"
#include <stddef.h>


// logic handler defined in logic library
extern void logic_handleCommand(ILBMessageStruct* m, uint8_t addressIndex);

void core_commandHandler(ILBMessageStruct *m)
{
	// forward to logic
	logic_handleCommand(m, addressing_getIndexOfAddress(m->address));
}

void core_readHandler(ILBMessageStruct *m)
{

}

void core_writeHandler(ILBMessageStruct *m)
{

}

/**
 * Send the start operation message
 */
void messaging_sendStartOperation(void)
{
	ILBMessageStruct msg;
	msg.address = ILB_MESSAGE_BROADCAST_ADDRESS;
	msg.identifier = ILB_GENERATE_IDENTIFIER;
	msg.messageType = ILB_MESSAGE_COMMAND;
	msg.payloadSize = 1;
	msg.payload[0] = ILB_START_OPERATION;
	bsw_messaging_sendMessage(&msg, NULL, 0, NULL);
}

//
//	Interface functions for logic
//
/**
 * Passthrough for code respond message
 *
 * @param msg
 * @param code
 * @return
 */
ILB_HAL_StatusTypeDef coreLib_respondWithCode(ILBMessageStruct* msg, ILBErrorTypeDef code)
{
	return bsw_messaging_respondCode(msg, code);
}

/**
 * Passthrough for sending messages
 *
 * @param msg
 * @param callback
 * @param timeoutMS
 * @param timeoutCallback
 * @return
 */
ILB_HAL_StatusTypeDef coreLib_sendMessage(ILBMessageStruct* msg, messageHandler_t callback, uint16_t timeoutMS, callbackHandle_t timeoutCallback)
{
	return bsw_messaging_sendMessage(msg, callback, timeoutMS, timeoutCallback);
}

#endif /* CORE_MESSAGING_C_ */
