/**
 * @file messaging.h
 *
 * @date Sep 27, 2021
 * @date A.Niggebaum
 *
 * @brief Message management functions
 *
 * @ingroup CoreBaseGroup
 */

#ifndef CORE_MESSAGING_MESSAGING_H_
#define CORE_MESSAGING_MESSAGING_H_

#include "ilb.h"

// function called by base software
/**
 * Function called by the base software when a command is received. The interface between base software and logic points to this routine
 * @param m Pointer to message received
 */
void core_commandHandler(ILBMessageStruct *m);

/**
 * Function called by the base software when a read message is received. The interface between the base software and the logic points to this routine
 * @param m Pointer to read message received
 */
void core_readHandler(ILBMessageStruct *m);

/**
 * Function called by the base software when a write message is received. The interface between the base software and the logic points to this routine
 * @param m Pointer to write message received
 */
void core_writeHandler(ILBMessageStruct *m);

// functions called from other areas in the library
// the initialisation function will sort out pointers to these functions
/**
 * Base software routine to send a message with payload. The initialization routine for the interface will populate the correct routine location.
 */
extern sendMessage_t bsw_messaging_sendMessage;

/**
 * Base software routine to respond to a message with an error or success code (single byte). The initialization rtouine for the interface will populate the correct routien location
 */
extern sendResponse_t bsw_messaging_respondCode;

//
//	Library internal functions
//
/**
 * Sends the ::ILB_START_OPERATION broadcast message, indicating to all peripherals that the core is now ready to receive and handle messages.
 */
void messaging_sendStartOperation(void);

#endif /* CORE_MESSAGING_MESSAGING_H_ */
