/*
 * settings.h
 *
 *  Created on: 28.09.2021
 *      Author: A.Niggebaum
 *
 * contains all releveant settings of library
 *
 */

#ifndef SETTINGS_SETTINGS_H_
#define SETTINGS_SETTINGS_H_

//
//	Network discovery and build up
//
#define SETTING_MAXIMAL_NUMBER_OF_PERIPHERALS		20
#define SETTING_NETWORK_DISCOVERY_TIMEOUT_MS		500			//!< time given to peripherals to report their UIDs in ms
#define SETTING_PERIPHERAL_ADDRESS_MAX				0xFE		//!< max address for peripherals to be chosen by random number generator
#define SETTING_PERIPHERAL_ADDRESS_MIN				0x02		//!< min address for peripherals that can be chosen by random number generator

#define SETTING_NETWORK_APPLY_SETTING_TIMEOUT_MS	300			//!< time given to peripherals to respond to a write command of settings

//
//	Network maintenance
//
#define SETTING_PERIODIC_NETWORK_CHECK_INTERVAL_MS	30000		//!< periodic network check
#define SETTING_NETWORK_CHECK_TIMEOUT_MS			500
#define SETTING_NETWORK_CHECK_TRACKER_READ_ATTEMPTS	3

//
//	Communication
//
#define SETTING_PERIPHERAL_DRIVER_COMMUNICATION_TIMEOUT_MS	500
#define SETTING_PERIPHERAL_BOOT_ALLOWANCE_MS		300			//!< delay after core boot before the normal operation starts

//
//	Broadcast messages
//
#define SETTINGS_BROADCAST_INTERVAL_S				30

//
//	Persistent variables
//
#define SETTING_MAX_NOF_PERSISTENT_LOGIC_VARS		20			//!< maximal number of persistent variables

//
//	Information Distribution interval
//
#define SETTINGS_INFORMATION_DISTRIBUTION_INTERVAL_S	60

#endif /* SETTINGS_SETTINGS_H_ */
